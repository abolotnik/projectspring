package com.water.repository.util;



import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateUtil {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            return new Configuration().configure().buildSessionFactory();

        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }
    /*private  static final Logger logger = Logger.getLogger(HibernateUtil.class);
//    private  static final  SessionFactory sessionfactory = buildFactory();
    private  static final  Session session = buildSession();

    public static Session buildSession(){
        try
        {
            logger.info("buildFacotry Session");
            return new Configuration().configure("E:\\FS_JD\\Project\\rebuild\\watershopapp\\repositories\\src\\main\\resources\\hibernate.cfg.xml").buildSessionFactory().openSession();
        }
        catch (Throwable e) {
            throw new ExceptionInInitializerError(e);
        }
    }

   *//* public static SessionFactory buildFactory(){
        try
        {
            logger.info("buildFacotry Session");
            return new Configuration().configure("E:\\FS_JD\\Project\\rebuild\\watershopapp\\repositories\\src\\main\\resources\\hibernate.cfg.xml").buildSessionFactory();
        }
        catch (Throwable e) {
            throw new ExceptionInInitializerError(e);
        }
    }*//*

    *//*public static SessionFactory getSessionfactory() {
        logger.info("return SessionFactory");
        return sessionfactory;
    }*//*

    public static Session getSession() {
        return session;
    }

   *//* public static void shutdown (){
        logger.info("Get SessionFactory");
        getSessionfactory().close();
    }*/

}

