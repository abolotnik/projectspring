package com.water.repository.util;


public enum Role {

    ROLE_SUPERADMIN,
    ROLE_ADMIN,
    ROLE_USER

}
