package com.water.repository.DAO;


import com.water.repository.model.ItemPojo;

import java.util.List;

public interface ItemDao extends GenericDao<ItemPojo, Integer> {
    List<ItemPojo> getUsersBasket(Integer userId);

    void deleteItemById(ItemPojo itemPojo) throws Exception;

    void updateItem(ItemPojo itemPojo);

    void addItem(ItemPojo itemPojo);
}
