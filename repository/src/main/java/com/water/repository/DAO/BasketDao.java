package com.water.repository.DAO;

import com.water.repository.model.BasketPojo;

import java.util.List;


public interface BasketDao extends GenericDao<BasketPojo, Integer> {

    void addItemToBasket(BasketPojo basketPojo);
    List<BasketPojo> getUsersBasket(Integer userId);
    void deleteItemFormBasket(BasketPojo basketPojo);



}
