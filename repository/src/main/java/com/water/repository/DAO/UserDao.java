package com.water.repository.DAO;


import com.water.repository.model.UserPojo;

import java.util.List;


public interface UserDao extends GenericDao<UserPojo, Integer>  {


    UserPojo getUserByEmail(String email);


    void registerNewUser(UserPojo userPojo);

    UserPojo getAllInfo(Integer id);

    //    List<ItemPojo> getUsersBasket(Integer userID);
    List<UserPojo> getAllUser();

    void deleteById(UserPojo userPojo);

    void updateUser(UserPojo userPojo);



}
