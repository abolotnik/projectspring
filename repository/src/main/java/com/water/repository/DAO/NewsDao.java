package com.water.repository.DAO;


import com.water.repository.model.NewsPojo;

import java.util.List;

public interface NewsDao extends GenericDao<NewsPojo, Integer> {

    List<NewsPojo> getAllNews();

    NewsPojo getImagesUrlById(Integer id);

    void deleteFromNews(NewsPojo newsPojo);

    void editNews(NewsPojo newsPojo);

}
