package com.water.repository.DAO;


import com.water.repository.model.OrderPojo;
import com.water.repository.model.UserPojo;

import java.util.List;


public interface OrderDao {


    void createNewOrder(OrderPojo order);

    List<OrderPojo> getAllOrderByUserId(Integer id);

    OrderPojo getOrderByUserId(Integer id);

    void deleteOrderById(Integer id);

    void deleteOrderByUser(UserPojo userPojo);
}
