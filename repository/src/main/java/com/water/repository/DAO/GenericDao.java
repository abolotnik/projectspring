package com.water.repository.DAO;



import java.io.Serializable;
import java.util.List;

public interface GenericDao <T extends Serializable, ID extends Serializable>{

    ID save ( T entity);

    void update ( T entity);

    void delete ( T entity);

    void deleteAll( );

    T findById( ID id);

    List<T> findAll ();
}
