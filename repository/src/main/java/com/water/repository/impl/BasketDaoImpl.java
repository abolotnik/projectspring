package com.water.repository.impl;

import com.water.repository.DAO.BasketDao;
import com.water.repository.model.BasketPojo;
import com.water.repository.model.UserPojo;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BasketDaoImpl extends GenericDaoImpl<BasketPojo, Integer> implements BasketDao {

    private  static final Logger logger = Logger.getLogger(BasketDaoImpl.class);




    @Override
    public void addItemToBasket(BasketPojo basketPojo) {
        logger.info("Add new row in Basket for user "+basketPojo.getUser().getId());
        getSession().merge(basketPojo);
    }

    @Override
    public List<BasketPojo> getUsersBasket(Integer userId) {
        logger.info("Get all items from Basket for user "+userId);
        String hql = "FROM BasketPojo B WHERE user.id=:userId AND IS_ACTIVE=:isActive";
        Query query = getSession().createQuery(hql);
        query.setParameter("userId", userId);
        query.setParameter("isActive", 1);
        List<BasketPojo> baskets = (List<BasketPojo>) query.list();
        return baskets;
    }

    @Override
    public void deleteItemFormBasket(BasketPojo basketPojo) {
        logger.info("Delete row in Basket " +basketPojo);
        getSession().delete(basketPojo);
    }
}
