package com.water.repository.impl;


import com.water.repository.DAO.ItemDao;
import com.water.repository.model.ItemPojo;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ItemDaoImpl extends GenericDaoImpl<ItemPojo, Integer> implements ItemDao {

    private static final Logger logger = Logger.getLogger(ItemDaoImpl.class);


    @Override
    public List<ItemPojo> getUsersBasket(Integer userId) {
        logger.info("Get all news from  Service Layer");
        String hql = "FROM ItemPojo I where I.users.id=:userId";
        Query query = getSession().createQuery(hql);
        query.setParameter("userId", userId);
        List<ItemPojo> basket = (List<ItemPojo>) query.list();
        logger.info("Get News For ");
        return basket != null ? basket : null;
    }

    @Override
    public void deleteItemById(ItemPojo itemPojo) throws Exception {
        logger.info("Delete row in Catalog " +itemPojo.getId());
        try {
            getSession().delete(itemPojo);
        } catch (DataIntegrityViolationException e){
            throw new Exception("Item not might been deleting");
        }
    }

    @Override
    public void updateItem(ItemPojo itemPojo) {
        logger.info("Update item with id "+itemPojo.getId());
        getSession().merge(itemPojo);
    }

    @Override
    public void addItem(ItemPojo itemPojo) {
        logger.info("Add item with id "+itemPojo.getId());
        getSession().save(itemPojo);
    }

}
