package com.water.repository.impl;

import com.water.repository.DAO.NewsDao;
import com.water.repository.model.NewsPojo;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Set;


@Repository
public class NewsDaoImpl extends GenericDaoImpl<NewsPojo, Integer> implements NewsDao {

    private static final Logger logger = Logger.getLogger(NewsDaoImpl.class);


    @Override
    public NewsPojo findById(Integer id) {
        return super.findById(id);
    }

    @Override
    public List<NewsPojo> getAllNews() {
        String hql = "FROM NewsPojo";
        Query query = getSession().createQuery(hql);
        List<NewsPojo> newsPojoList = (List<NewsPojo>) query.list();
        logger.info("Get News For ");
        return newsPojoList != null ? newsPojoList : null;

    }

    @Override
    public NewsPojo getImagesUrlById(Integer id) {
        return null;
    }

    @Override
    public void deleteFromNews(NewsPojo newsPojo) {
        logger.info("Delete row in News " +newsPojo);
        getSession().delete(newsPojo);
    }

    @Override
    public void editNews(NewsPojo newsPojo) {
        logger.info("Edit news with id "+newsPojo.getId());
        getSession().merge(newsPojo);
    }


}
