package com.water.repository.impl;

import com.water.repository.DAO.OrderDao;
import com.water.repository.model.OrderPojo;
import com.water.repository.model.UserPojo;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderDaoImpl extends GenericDaoImpl<OrderPojo, Integer> implements OrderDao {
    private static final Logger logger = Logger.getLogger(OrderDaoImpl.class);


    @Override
    public void createNewOrder(OrderPojo order) {
        logger.info("Create new Order for User");
        getSession().save(order);
    }

    @Override
    public List<OrderPojo> getAllOrderByUserId(Integer id) {
        logger.info("Get order for User "+id);
        String hql = "FORM OrderPojo P WHERE UserPojo.id=:id";
        Query query = getSession().createQuery(hql);
        query.setParameter("id", id);
        List<OrderPojo> orderPojoList = (List<OrderPojo>)query.list();

        return null;
    }

    @Override
    public OrderPojo getOrderByUserId(Integer id) {
        return null;
    }

    @Override
    public void deleteOrderById(Integer id) {

    }

    @Override
    public void deleteOrderByUser(UserPojo userPojo) {

    }
}
