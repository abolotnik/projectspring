package com.water.repository.impl;


import com.water.repository.DAO.GenericDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@SuppressWarnings("unchecked")
@Repository
public abstract class GenericDaoImpl<T extends Serializable, ID extends Serializable> implements GenericDao<T, ID> {


    @Autowired
    private SessionFactory sessionFactory;

    private Class<T> entityClass;

    public GenericDaoImpl() {
        this.entityClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public ID save(T entity) {
        return (ID) getSession().save(entity);
    }

    @Override
    public void update(T entity) {
        getSession().merge(entity);
    }

    @Override
    public void delete(T entity) {
        getSession().delete(entity);
    }

    @Override
    public void deleteAll() {
        List<T> entities = findAll();
        for (T entity : entities) {
            getSession().delete(entity);
        }

    }

    @Override
    public T findById(ID id) {
        return (T) getSession().get(this.entityClass, id);
    }

    @Override
    public List<T> findAll() {
        return getSession().createCriteria(this.entityClass).list();
    }
}
