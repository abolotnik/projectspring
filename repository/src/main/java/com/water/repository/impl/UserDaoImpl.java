package com.water.repository.impl;


import com.water.repository.DAO.UserDao;
import com.water.repository.model.ItemPojo;
import com.water.repository.model.UserPojo;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImpl extends GenericDaoImpl<UserPojo, Integer> implements UserDao {

    private  static final Logger logger = Logger.getLogger(UserDaoImpl.class);


    @Override
    public  UserPojo getUserByEmail( String email) throws NullPointerException {
        logger.info("---------------------getUserByEmail-----------------------");
        String hql = "FROM UserPojo where email=:email";
        Query query = getSession().createQuery(hql);
        query.setParameter("email", email);
        UserPojo user = (UserPojo) query.uniqueResult();
        return user != null ?  user : null;
    }

    @Override
    public void registerNewUser(UserPojo userPojo) {
        logger.info("-----------------Saving user---------------------");
        getSession().save(userPojo);
        logger.info("-------------------saving user finished-----------------");
    }

    @Override
    public UserPojo getAllInfo(Integer id) {
        logger.info("Get all info for userId "+id);
        UserPojo user = UserPojo.getInstance();
        user = (UserPojo)getSession().load(UserPojo.class, id);
        return user;
    }

    @Override
    public List<UserPojo> getAllUser() {
        return null;
    }

    @Override
    public void deleteById(UserPojo userPojo) {

    }

    @Override
    public void updateUser(UserPojo userPojo) {

    }

//    @Override
//    public List<ItemPojo> getUsersBasket(Integer userId) {
//        logger.info("Get all news from  Service Layer");
//        UserPojo user = getSession().load(UserPojo.class, userId);
//        List<ItemPojo> basket = user.getItems();
//        logger.info("Get News For ");
//        return basket != null ? basket : null;
//    }


}
