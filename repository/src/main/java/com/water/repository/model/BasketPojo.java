package com.water.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "T_BASKET")
public class BasketPojo implements Serializable {


    private static final long serialVersionUID = 8061728821461935974L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;

    @Column(name = "F_NAME")
    private String name;

    @Column(name = "F_PHOTO_URL")
    private String photoUrl;

    @Column(name = "F_DESCRIPTION")
    private String description;

    @Column(name = "F_PRICE")
    private Double price;

    @Column(name = "F_COUNT")
    private Integer count;

    @Column(name = "IS_ACTIVE")
    private boolean isActive;

    public BasketPojo() {
    }

    @ManyToOne
    @JoinColumn(name = "F_USER_ID")
    private UserPojo user;

    @ManyToOne
    @JoinColumn(name = "F_ITEM_ID")
    private ItemPojo item;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "T_ORDER_TRANSP",
            joinColumns = @JoinColumn(name = "F_BASKET_ID"),
            inverseJoinColumns = @JoinColumn(name = "F_ORDER_ID"))
    private List<OrderPojo> orders;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public UserPojo getUser() {
        return user;
    }

    public void setUserPojo(UserPojo user) {
        this.user = user;
    }

    public void setUser(UserPojo user) {
        this.user = user;
    }

    public ItemPojo getItem() {
        return item;
    }

    public void setItem(ItemPojo item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasketPojo that = (BasketPojo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(photoUrl, that.photoUrl) &&
                Objects.equals(description, that.description) &&
                Objects.equals(user, that.user) &&
                Objects.equals(item, that.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, photoUrl, description, user, item);
    }
}
