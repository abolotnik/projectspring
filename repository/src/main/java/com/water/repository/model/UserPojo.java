package com.water.repository.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.*;


@Entity
@Table(name = "T_USER")
public class UserPojo implements Serializable {

    private static final long serialVersionUID = 5386076361903975604L;

    private static UserPojo instance = new UserPojo();

    public static UserPojo getInstance() {
        if (instance == null) {
            instance = new UserPojo();
            return instance;
        }
        return instance;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;

    @Column(name = "F_NAME")
    private String name;

    @Column(name = "F_SURNAME")
    private String surname;

    @Column(name = "F_EMAIL")
    private String email;

    @Column(name = "F_PASSWORD")
    private String password;

    @Column(name = "F_ROLE")
    private String role;

    @Column(name = "IS_ACTIVE")
    private boolean status;


    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<NewsPojo> news;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<AddressPojo> addresses ;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<PhonePojo> phones ;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<ContactPojo> contacts ;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<OrderPojo> orders;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<BasketPojo> basket ;

    private UserPojo() {
    }

    public UserPojo(String name, String surname, String email, String password, String role) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<NewsPojo> getNews() {
        return news;
    }

    public List<AddressPojo> getAddresses() {
        return addresses;
    }

    public List<PhonePojo> getPhones() {
        return phones;
    }

    public List<ContactPojo> getContacts() {
        return contacts;
    }

    public List<OrderPojo> getOrders() {
        return orders;
    }

    public List<BasketPojo> getBasket() {
        return basket;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setNews(List<NewsPojo> news) {
        this.news = news;
    }

    public void setAddresses(List<AddressPojo> addresses) {
        this.addresses = addresses;
    }

    public void setPhones(List<PhonePojo> phones) {
        this.phones = phones;
    }

    public void setContacts(List<ContactPojo> contacts) {
        this.contacts = contacts;
    }

    public void setOrders(List<OrderPojo> orders) {
        this.orders = orders;
    }

    public void setBasket(List<BasketPojo> basket) {
        this.basket = basket;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPojo userPojo = (UserPojo) o;
        return Objects.equals(id, userPojo.id) &&
                Objects.equals(email, userPojo.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email);
    }



}
