package com.water.repository.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Table(name = "T_NEWS")
public class NewsPojo implements Serializable {

    private static final long serialVersionUID = 3833645619308758252L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;

    @Column(name = "F_TITLE")
    private String title;

    @Column(name = "F_PHOTO_URL")
    private String photoUrl;

    @Column(name = "F_CONTENT")
    private String newsContent;

    @ManyToOne
    @JoinColumn(name = "F_USER_ID")
    private UserPojo user;

    public NewsPojo() {
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getNewsContent() {
        return newsContent;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public void setNewsContent(String newsContent) {
        this.newsContent = newsContent;
    }

    public UserPojo getUser() {
        return user;
    }

    public void setUser(UserPojo user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewsPojo newsPojo = (NewsPojo) o;
        return Objects.equals(id, newsPojo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
