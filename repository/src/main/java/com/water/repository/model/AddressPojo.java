package com.water.repository.model;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "T_ADDRESS")
public class AddressPojo implements Serializable{


    private static final long serialVersionUID = 3480851435966948040L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "F_USER_ID")
    private UserPojo user;

    @Column(name = "F_POST_CODE")
    private Integer postCode;

    @Column(name = "F_COUNTRY")
    private String country;

    @Column(name = "F_CITY")
    private String city;

    @Column(name = "F_STREET")
    private String street;

    @Column(name = "F_BUILDING")
    private Integer building;

    @Column(name = "F_APARTAMENT")
    private Integer apartament;


    @OneToMany(mappedBy = "address", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<OrderPojo> orders ;


    public AddressPojo() {
    }

    public AddressPojo(Integer postCode, String country,
                       String city, String street,
                       Integer building, Integer apartament) {
        this.postCode = postCode;
        this.country = country;
        this.city = city;
        this.street = street;
        this.building = building;
        this.apartament = apartament;
    }

    public Integer getId() {
        return id;
    }

    public Integer getPostCode() {
        return postCode;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public Integer getBuilding() {
        return building;
    }

    public Integer getApartament() {
        return apartament;
    }

    public UserPojo getUser() {
        return user;
    }

    public List<OrderPojo> getOrders() {
        return orders;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPostCode(Integer postCode) {
        this.postCode = postCode;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setBuilding(Integer building) {
        this.building = building;
    }

    public void setApartament(Integer apartament) {
        this.apartament = apartament;
    }

    public void setUser(UserPojo user) {
        this.user = user;
    }

    public void setOrders(List<OrderPojo> orders) {
        this.orders = orders;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressPojo that = (AddressPojo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(postCode, that.postCode) &&
                Objects.equals(country, that.country) &&
                Objects.equals(city, that.city) &&
                Objects.equals(street, that.street) &&
                Objects.equals(building, that.building) &&
                Objects.equals(apartament, that.apartament);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, postCode, country, city, street, building, apartament);
    }
}
