package com.water.repository.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "T_USER_CONTACT")

public class ContactPojo implements Serializable{

    private static final long serialVersionUID = -438589919064406207L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;

    @Column(name = "F_NAME")
    private String name;

    @Column(name = "F_SURNAME")
    private String surname;

    @Column(name = "F_PHONE")
    private String phone;

    @ManyToOne
    @JoinColumn(name = "F_USER_ID")
    private UserPojo user;

    @OneToMany(mappedBy = "address", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    List<OrderPojo> orders ;


    public ContactPojo() {
    }

    public ContactPojo(String name, String surname, String phone) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
    }


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhone() {
        return phone;
    }

    public UserPojo getUser() {
        return user;
    }

    public List<OrderPojo> getOrders() {
        return orders;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUser(UserPojo user) {
        this.user = user;
    }

    public void setOrders(List<OrderPojo> orders) {
        this.orders = orders;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContactPojo that = (ContactPojo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(phone, that.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, phone);
    }
}
