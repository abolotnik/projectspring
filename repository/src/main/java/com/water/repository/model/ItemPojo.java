package com.water.repository.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.*;


@Entity
@Table(name = "T_ITEM")
public class ItemPojo implements Serializable {

    private static final long serialVersionUID = -7705739962017918542L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;

    @Column(name = "F_NAME")
    private String name;

    @Column(name = "F_PHOTO_URL")
    private String photoUrl;

    @Column(name = "F_DESCRIPTION")
    private String description;

    @Column(name = "F_PRICE")
    private Double price;

    @Column(name = "F_COUNT")
    private Integer count;


    @OneToMany(mappedBy = "item", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<BasketPojo> basket ;



    public ItemPojo() {
    }

    public ItemPojo(String name, String photoUrl, String description, Double price, Integer count) {
        this.name = name;
        this.photoUrl = photoUrl;
        this.description = description;
        this.price = price;
        this.count = count;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {
        return price;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemPojo itemPOJO = (ItemPojo) o;
        return Objects.equals(id, itemPOJO.id) &&
                Objects.equals(name, itemPOJO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
