package com.water.repository.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "T_PHONE")
public class PhonePojo implements Serializable{

    private static final long serialVersionUID = 1747065116040860090L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;

    @Column(name = "F_PHONE")
    private String phone;

    @ManyToOne
    @JoinColumn(name = "F_USER_ID")
    private UserPojo user;

    public PhonePojo() {
    }

    public PhonePojo(String phone) {
        this.phone = phone;
    }

    public Integer getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public UserPojo getUser() {
        return user;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUser(UserPojo user) {
        this.user = user;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhonePojo phonePOJO = (PhonePojo) o;
        return Objects.equals(id, phonePOJO.id) &&
                Objects.equals(phone, phonePOJO.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phone);
    }
}
