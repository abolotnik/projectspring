package com.water.repository.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "T_ORDER")
public class OrderPojo implements Serializable {

    private static final long serialVersionUID = -8542631818198243187L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;

    @Column(name = "F_PRICE_ORDER")
    private Double priceOrder;

    @Column(name = "F_STATUS")
    private String status;

    @Column(name = "F_COMMENT")
    private String comment;

    @ManyToOne
    @JoinColumn(name = "F_USER_ID")
    private UserPojo user;

    @ManyToOne
    @JoinColumn(name = "F_ADDRESS_ID")
    private AddressPojo address;

    @ManyToOne
    @JoinColumn(name = "F_CONTACT_ID")
    private ContactPojo contact;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "T_ORDER_TRANSP",
            joinColumns = @JoinColumn(name = "F_ORDER_ID"),
            inverseJoinColumns = @JoinColumn(name = "F_BASKET_ID"))
    private List<BasketPojo> baskets ;

    public OrderPojo() {
    }

    public Integer getId() {
        return id;
    }

    public Double getPriceOrder() {
        return priceOrder;
    }

    public String getStatus() {
        return status;
    }

    public UserPojo getUser() {
        return user;
    }

    public AddressPojo getAddress() {
        return address;
    }

    public ContactPojo getContact() {
        return contact;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPriceOrder(Double priceOrder) {
        this.priceOrder = priceOrder;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setUser(UserPojo user) {
        this.user = user;
    }

    public void setAddress(AddressPojo address) {
        this.address = address;
    }

    public void setContact(ContactPojo contact) {
        this.contact = contact;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<BasketPojo> getBaskets() {
        return baskets;
    }

    public void setBaskets(List<BasketPojo> baskets) {
        this.baskets = baskets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderPojo orderPOJO = (OrderPojo) o;
        return Objects.equals(id, orderPOJO.id) &&
                Objects.equals(priceOrder, orderPOJO.priceOrder) &&
                Objects.equals(status, orderPOJO.status) &&
                Objects.equals(user, orderPOJO.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, priceOrder, status, user);
    }
}
