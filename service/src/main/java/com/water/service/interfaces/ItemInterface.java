package com.water.service.interfaces;


import com.water.service.model.ItemsDTO;

import java.util.List;

public interface ItemInterface {
    List<ItemsDTO> getCatalog();
//    Integer addItemToBasket( ItemsDTO itemsDTO);
    ItemsDTO getById(Integer id);

    void deleteById(Integer id) throws Exception;

    void editItem(ItemsDTO itemsDTO);

    void addItem(ItemsDTO itemsDTO);
}
