package com.water.service.interfaces;


import com.water.service.model.BasketDTO;

import java.util.List;

public interface BasketInterface {

    List<BasketDTO> getBasket();

    void addItemToBasket(BasketDTO basketDTO);

    BasketDTO getById(Integer id);

    void deleteById(Integer itemId);

    void changeBasketById(BasketDTO basketDTO);

}
