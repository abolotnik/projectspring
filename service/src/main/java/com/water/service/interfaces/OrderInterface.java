package com.water.service.interfaces;


import com.water.repository.model.OrderPojo;
import com.water.repository.model.UserPojo;
import com.water.service.model.OrderDTO;

import java.util.List;

public interface OrderInterface {



    void createNewOrder(OrderDTO orderDTO);

    List<OrderPojo> getAllOrderByUserId(Integer id);

    OrderPojo getOrderByUserId(Integer id);

    void deleteOrderById(Integer id);

    void deleteOrderByUser(UserPojo userPojo);
}
