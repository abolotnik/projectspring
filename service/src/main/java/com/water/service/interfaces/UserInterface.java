package com.water.service.interfaces;


import com.water.repository.model.UserPojo;
import com.water.service.model.ItemsDTO;
import com.water.service.model.UserDTO;

import java.util.List;

public interface UserInterface {

    boolean checkUserByEmail(String email, String password);

    UserDTO getUserByEmail(UserDTO userDTO);

    void saveUser(UserDTO userDTO);

    List<ItemsDTO> getBasket();

    UserPojo getUser();

    List<UserDTO> getAllUser();

    void deleteUserById(Integer id);

    UserDTO getUserById(Integer id);

    void changePassword(UserDTO userDTO);

    void changeRole(UserDTO userDTO);

}
