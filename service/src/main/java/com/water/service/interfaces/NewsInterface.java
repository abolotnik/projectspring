package com.water.service.interfaces;


import com.water.service.model.NewsDTO;

import java.util.List;

public interface NewsInterface {

    List<NewsDTO> getAllNews();

    List<NewsDTO> getImagesById();

    void addNews(NewsDTO newsDTO);

    void deleteById(Integer id);

    NewsDTO getNewsById(Integer id);

    void editNews(NewsDTO newsDTO);


}
