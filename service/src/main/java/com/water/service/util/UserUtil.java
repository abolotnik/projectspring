package com.water.service.util;

import com.water.repository.impl.UserDaoImpl;
import com.water.repository.model.UserPojo;
import com.water.repository.util.HibernateUtil;
import com.water.service.converter.Converter;
import com.water.service.model.ItemsDTO;
import com.water.service.model.UserDTO;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import java.util.Set;


public class UserUtil {

    private static final Logger logger = Logger.getLogger(UserUtil.class);
    private static UserUtil instance = new UserUtil();

    public static UserUtil getInstance() {
        if (instance == null) {
            instance = new UserUtil();
            return instance;
        }
        return instance;
    }

    public boolean checkUserByEmail(String email, String password) {

        UserDTO user = UserUtil.getInstance().getUserByEmail(email);
        if (user != null) {
            if (email.equals(user.getEmail()) && password.equals(user.getPassword())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }


    }

    public UserDTO getUserByEmail(String email) {
        UserDaoImpl userDao = new UserDaoImpl();
        UserDTO userDTO = null;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        try {
            UserPojo userPojo = userDao.getUserByEmail( email);
            userDTO = Converter.convertUser(userPojo);
        } catch (NullPointerException e) {
            logger.info("user with email " + email + " not registred");
            userDTO = null;
        }
        session.getTransaction().commit();

        return userDTO;
    }

    public static UserPojo setUserPojo(UserDTO user) {
        UserPojo userPojo = UserPojo.getInstance();

        userPojo.setEmail(user.getEmail());
        userPojo.setPassword(user.getPassword());
        userPojo.setName(user.getName());

        return userPojo;
    }

    public void saveUser(UserDTO userDTO) {
        UserDaoImpl daoImpl = new UserDaoImpl();
        UserPojo userPojo = UserPojo.getInstance();

        logger.info("Session create");
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        logger.info("begin transaction");
        session.beginTransaction();

        userPojo = Converter.convert(userDTO);
        daoImpl.registerNewUser(userPojo);

        session.getTransaction().commit();
        logger.info("transaction commit");
        try {
            if (session.isOpen()) {
                session.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Double getTotalPrice(Set<ItemsDTO> itemsDTOSet){
        Double totalPrice = null;
        for (ItemsDTO itemsDTO : itemsDTOSet) {
            totalPrice = itemsDTO.getPrice();
        }

        return  totalPrice;
    }


}
