package com.water.service.impl;

import com.water.repository.DAO.UserDao;
import com.water.repository.model.UserPojo;
import com.water.repository.util.Role;
import com.water.service.converter.Converter;
import com.water.service.converter.UserConverter;
import com.water.service.interfaces.UserInterface;
import com.water.service.model.AppUserPrincipal;
import com.water.service.model.ItemsDTO;
import com.water.service.model.UserDTO;
import com.water.service.util.UserUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserInterfaceImpl implements UserInterface {



    private final UserDao userDao;
    private final PasswordEncoder passwordEncoder;
    private static final Logger logger = Logger.getLogger(UserUtil.class);

    @Autowired
    public UserInterfaceImpl(
            UserDao userDao,
            PasswordEncoder passwordEncoder
    ) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }




    @Override
    @Transactional
    public boolean checkUserByEmail(String email, String password) {
        UserPojo user = (UserPojo) userDao.getUserByEmail(email);
        if (user == null) {
                return true;
        } else {
            return false;
        }
    }


    @Override
    @Transactional
    public UserDTO getUserByEmail(UserDTO user) {
        if (checkUserByEmail(user.getEmail(), user.getPassword())) {
            logger.info("---------------Service getUserByEmail-----------------");
            UserDTO userDTO = Converter.convertUser(userDao.getUserByEmail(user.getEmail()));
            return userDTO;
        } else {
            logger.info("user with email " + user.getEmail() + " not registred");
            return null;
        }
    }


    @Override
    @Transactional
    public void saveUser(UserDTO userDTO) {
        logger.info("Save user");
        UserPojo userPojo = Converter.convert(userDTO);
        userPojo.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        userDao.registerNewUser(userPojo);
    }

    @Override
    @Transactional
    public List<ItemsDTO> getBasket() {
//        UserPojo userPojo = getUser();
//        List<ItemsDTO> basket = Converter.convertListItems(userDao.getUsersBasket(userPojo.getId()));
        return null;
    }

    public UserPojo getUser() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return userDao.findById(principal.getUserId());
    }

    @Override
    @Transactional
    public List<UserDTO> getAllUser() {
        logger.info("Get All Users");
        List<UserDTO> users = new ArrayList<>();
        users = UserConverter.convert(userDao.getAllUser());
        return users;
    }

    @Override
    @Transactional
    public void deleteUserById(Integer id) {
        logger.info("Delete Users with id "+id);
        UserPojo userPojo = userDao.findById(id);
        userDao.deleteById(userPojo);
    }

    @Override
    @Transactional
    public UserDTO getUserById(Integer id) {
        UserDTO userDTO = UserConverter.convert(userDao.findById(id));
        return userDTO;
    }

    @Override
    @Transactional
    public void changePassword(UserDTO userDTO) {
        UserPojo userPojo = userDao.getUserByEmail(userDTO.getEmail());
        userPojo.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        userPojo.setRole(Role.ROLE_ADMIN.name());
        userDao.updateUser(userPojo);
    }

    @Override
    @Transactional
    public void changeRole(UserDTO userDTO) {
        UserPojo userPojo = userDao.getUserByEmail(userDTO.getEmail());
        userPojo.setRole(userDTO.getRole());
        userDao.updateUser(userPojo);

    }
}
