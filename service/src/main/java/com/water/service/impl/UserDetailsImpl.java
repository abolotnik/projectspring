package com.water.service.impl;

import com.water.repository.DAO.UserDao;
import com.water.repository.model.UserPojo;
import com.water.service.model.AppUserPrincipal;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "userDetailsService")
public class UserDetailsImpl implements UserDetailsService {

    private final UserDao userDao;
    private static final Logger logger = Logger.getLogger(UserDetailsImpl.class);

    @Autowired
    public UserDetailsImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        logger.info("------------------------Load User by email: "+email+"------------------------");
        UserPojo user = userDao.getUserByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        return new AppUserPrincipal(user);
    }
}
