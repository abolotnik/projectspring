package com.water.service.impl;

import com.water.repository.DAO.ItemDao;
import com.water.repository.DAO.UserDao;
import com.water.repository.model.ItemPojo;
import com.water.repository.model.UserPojo;
import com.water.service.converter.Converter;
import com.water.service.converter.ItemConverter;
import com.water.service.interfaces.ItemInterface;
import com.water.service.model.AppUserPrincipal;
import com.water.service.model.ItemsDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItemInterfaceImpl implements ItemInterface {
    @Qualifier("itemDao")
    private final ItemDao itemDao;
    private final UserDao userDao;
    private static final Logger logger = Logger.getLogger(ItemInterfaceImpl.class);

    @Autowired
    public ItemInterfaceImpl(ItemDao itemDao, UserDao userDao) {
        this.itemDao = itemDao;
        this.userDao = userDao;
    }

    private UserPojo getUser() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return userDao.findById(principal.getUserId());
    }

    @Override
    @Transactional
    public List<ItemsDTO> getCatalog() {
        List<ItemsDTO> catalog = ItemConverter.convertListItems(itemDao.findAll());
        return catalog;
    }

//    @Override
//    @Transactional
//    public Integer addItemToBasket( ItemsDTO itemsDTO) {
//        UserPojo userPojo = getUser();
//        ItemPojo itemPojo = Converter.convertItem(itemsDTO);
//        userPojo.getItems().add(itemPojo);
////        Integer id = itemDao.save(itemPojo);
//        Integer id = userDao.save(userPojo);
//        return id;
//    }

    @Override
    @Transactional
    public ItemsDTO getById(Integer id) {
        logger.info("Find item with ID:" + id);
        ItemsDTO itemsDTO = Converter.convertItem(itemDao.findById(id));
        return itemsDTO;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) throws Exception {
        logger.info("Delete from items  " + id);
        ItemPojo newsPojo = (ItemPojo) itemDao.findById(id);
        itemDao.deleteItemById(newsPojo);
    }

    @Override
    @Transactional
    public void editItem(ItemsDTO itemsDTO) {
        ItemPojo itemPojo = itemDao.findById(itemsDTO.getId());
        itemPojo.setPrice(itemsDTO.getPrice());
        itemDao.updateItem(itemPojo);
    }

    @Override
    @Transactional
    public void addItem(ItemsDTO itemsDTO) {
        ItemPojo itemPojo = ItemConverter.convertItem(itemsDTO);
        itemDao.addItem(itemPojo);
    }

}
