package com.water.service.impl;

import com.water.repository.DAO.BasketDao;
import com.water.repository.DAO.ItemDao;
import com.water.repository.DAO.UserDao;
import com.water.repository.model.BasketPojo;
import com.water.repository.model.ItemPojo;
import com.water.repository.model.UserPojo;
import com.water.service.converter.BasketConverter;
import com.water.service.converter.Converter;
import com.water.service.interfaces.BasketInterface;
import com.water.service.model.AppUserPrincipal;
import com.water.service.model.BasketDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BasketInterfaceImpl implements BasketInterface {

    private static final Logger logger = Logger.getLogger(BasketInterfaceImpl.class);


    private BasketDao basketDao;
    private UserDao userDao;
    private ItemDao itemDao;

    @Autowired
    public BasketInterfaceImpl(BasketDao basketDao, UserDao userDao, ItemDao itemDao) {
        this.basketDao = basketDao;
        this.userDao = userDao;
        this.itemDao = itemDao;
    }

    @Override
    @Transactional
    public List<BasketDTO> getBasket() {
        UserPojo userPojo = getUser();
        List<BasketDTO>  basketDTOList = BasketConverter.convertListBaskets(basketDao.getUsersBasket(userPojo.getId()));
        return basketDTOList;
    }

    @Override
    @Transactional
    public void addItemToBasket(BasketDTO basketDTO) {
        BasketPojo basketPojo = Converter.convertBasket(basketDTO);
        UserPojo userPojo = getUser();
        ItemPojo itemPojo = itemDao.findById(basketDTO.getId());
        basketPojo.setUserPojo(userPojo);
        basketPojo.setItem(itemPojo);
        basketDao.addItemToBasket(basketPojo);
    }

    @Override
    @Transactional
    public BasketDTO getById(Integer id) {
        logger.info("Get from basket item "+ id);
        BasketPojo basketPojo = (BasketPojo)basketDao.findById(id);
        BasketDTO basketDTO = BasketConverter.convertBasket(basketPojo);
        return basketDTO;
    }

    @Override
    @Transactional
    public void deleteById(Integer itemId) {
        logger.info("Delete from basket itme "+ itemId);
        BasketPojo basketPojo = (BasketPojo)basketDao.findById(itemId);
        basketDao.deleteItemFormBasket(basketPojo);
    }

    @Override
    @Transactional
    public void changeBasketById(BasketDTO basketDTO) {
        logger.info("Change row from basket "+ basketDTO.getId());
        BasketPojo basketPojo = basketDao.findById(basketDTO.getId());
        basketPojo.setCount(basketDTO.getCount());
        basketPojo.setPrice(basketPojo.getItem().getPrice()*basketDTO.getCount());
        basketDao.update(basketPojo);
    }


    public UserPojo getUser() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return userDao.findById(principal.getUserId());
    }
}
