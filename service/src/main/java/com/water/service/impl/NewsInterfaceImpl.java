package com.water.service.impl;

import com.water.repository.DAO.NewsDao;
import com.water.repository.DAO.UserDao;
import com.water.repository.model.NewsPojo;
import com.water.repository.model.UserPojo;
import com.water.service.converter.NewsConverter;
import com.water.service.interfaces.NewsInterface;
import com.water.service.model.AppUserPrincipal;
import com.water.service.model.NewsDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class NewsInterfaceImpl implements NewsInterface {

    private static final Logger logger = Logger.getLogger(NewsInterfaceImpl.class);
    @Autowired
    private NewsDao newsDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private Environment environment;


    @Override
    @Transactional
    public List<NewsDTO> getAllNews() {
        logger.info("Get all news from  Service Layer");
        List<NewsDTO> newsDTOList = new ArrayList<>();
        try {
            newsDTOList = NewsConverter.convertListNews(newsDao.getAllNews());
        } catch (NullPointerException e) {
            logger.error("No one News doesn't exists");
            newsDTOList = null;
        }
        return newsDTOList;
    }


    @Override
    public List<NewsDTO> getImagesById() {
        logger.error("No one News doesn't exists");
        List<NewsDTO> newsDTOList = NewsConverter.convertListNews(newsDao.getAllNews());
        return newsDTOList;
    }

    @Override
    @Transactional
    public void addNews(NewsDTO newsDTO) {
        NewsPojo newsPojo = NewsConverter.convertNews(newsDTO);
        try {
            newsPojo.setPhotoUrl(copyImages(newsDTO.getPhotoUrl()));
        } catch (IOException e) {
            logger.info("Some problem with coping files");
        }
        UserPojo userPojo = getUser();
        newsPojo.setUser(userPojo);
        newsDao.save(newsPojo);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        logger.info("Delete from news  "+ id);
        NewsPojo newsPojo = (NewsPojo) newsDao.findById(id);
        newsDao.deleteFromNews(newsPojo);
    }

    @Override
    @Transactional
    public NewsDTO getNewsById(Integer id) {
        NewsPojo newsPojo = newsDao.findById(id);
        return NewsConverter.convertNews(newsPojo);
    }

    @Override
    @Transactional
    public void editNews(NewsDTO newsDTO) {
        NewsPojo newsPojo = newsDao.findById(newsDTO.getId());
        newsPojo.setNewsContent(newsDTO.getContent());
        newsDao.editNews(newsPojo);
    }

    public String copyImages(String source) throws IOException {
        String dest = environment.getProperty("photo.url") + System.currentTimeMillis();
//        File fileSource = new File(source);
//        File fileDest = new File(dest);
//        FileCopyUtils.copy(fileSource, fileDest);
//        return fileDest.toString();
        return dest;
    }


    public UserPojo getUser() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return userDao.findById(principal.getUserId());
    }


}
