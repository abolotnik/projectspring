package com.water.service.impl;


import com.water.repository.DAO.BasketDao;
import com.water.repository.DAO.OrderDao;
import com.water.repository.DAO.UserDao;
import com.water.repository.model.BasketPojo;
import com.water.repository.model.OrderPojo;
import com.water.repository.model.UserPojo;
import com.water.service.interfaces.OrderInterface;
import com.water.service.model.AppUserPrincipal;
import com.water.service.model.OrderDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrderInterfaceImpl implements OrderInterface {

    private static final Logger logger = Logger.getLogger(OrderInterfaceImpl.class);
    private final OrderDao orderDao;
    private final UserDao userDao;
    private final BasketDao basketDao;

    @Autowired
    public OrderInterfaceImpl(OrderDao orderDao, UserDao userDao, BasketDao basketDao) {
        this.orderDao = orderDao;
        this.userDao = userDao;
        this.basketDao = basketDao;
    }

    @Override
    @Transactional
    public void createNewOrder(OrderDTO orderDTO) {
        UserPojo userPojo = getUser();
        List<BasketPojo> basket = basketDao.getUsersBasket(userPojo.getId());
        OrderPojo orderPojo = new OrderPojo();
        orderPojo.setUser(userPojo);
        orderPojo.setBaskets(basket);
        orderDao.createNewOrder(orderPojo);

    }


    @Override
    public List<OrderPojo> getAllOrderByUserId(Integer id) {
        return null;
    }

    @Override
    public OrderPojo getOrderByUserId(Integer id) {
        return null;
    }

    @Override
    public void deleteOrderById(Integer id) {

    }

    @Override
    public void deleteOrderByUser(UserPojo userPojo) {

    }

    private UserPojo getUser() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return userDao.findById(principal.getUserId());
    }
}
