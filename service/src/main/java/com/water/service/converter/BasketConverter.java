package com.water.service.converter;


import com.water.repository.model.BasketPojo;
import com.water.service.model.BasketDTO;

import java.util.ArrayList;
import java.util.List;

public class BasketConverter {

    public static BasketDTO convertBasket(BasketPojo basketPojo) {
        BasketDTO basketDTO = new BasketDTO();
        basketDTO.setId(basketPojo.getId());
        basketDTO.setName(basketPojo.getName());
        basketDTO.setPhotoUrl(basketPojo.getPhotoUrl());
        basketDTO.setDescription(basketPojo.getDescription());
        basketDTO.setCount(basketPojo.getCount());
        basketDTO.setPrice(basketPojo.getPrice());
        basketDTO.setUserId(basketPojo.getUser().getId());
        basketDTO.setItemId(basketPojo.getItem().getId());
        return basketDTO;
    }


    public static List<BasketDTO> convertListBaskets(List<BasketPojo> basketPojoList) {
        List<BasketDTO> basketDTOList = new ArrayList<>();
        for (BasketPojo basketPojo : basketPojoList) {
            basketDTOList.add(BasketConverter.convertBasket(basketPojo));
        }
        return basketDTOList;
    }


    public static BasketPojo convertBasket(BasketDTO basketDTO) {
        BasketPojo basketPojo = new BasketPojo();
        basketPojo.setName(basketDTO.getName());
        basketPojo.setDescription(basketDTO.getDescription());
        basketPojo.setPhotoUrl(basketDTO.getPhotoUrl());
        basketPojo.setPrice(basketDTO.getPrice());
        basketPojo.setCount(basketDTO.getCount());
        basketPojo.setActive(true);
        return basketPojo;
    }






}
