package com.water.service.converter;


import com.water.repository.model.ItemPojo;
import com.water.service.model.ItemsDTO;

import java.util.ArrayList;
import java.util.List;

public class ItemConverter {


    public static ItemsDTO convertItem(ItemPojo itemPojo) {
        ItemsDTO itemsDTO = ItemsDTO.newBuilder()
                .id(itemPojo.getId())
                .name(itemPojo.getName())
                .description(itemPojo.getDescription())
                .photoUrl(itemPojo.getPhotoUrl())
                .price(itemPojo.getPrice())
                .count(itemPojo.getCount())
                .build();
        return itemsDTO;
    }

    public static ItemPojo convertItem(ItemsDTO itemsDTO) {
        ItemPojo itemPojo = new ItemPojo();
        itemPojo.setName(itemsDTO.getName());
        itemPojo.setDescription(itemsDTO.getDescription());
        itemPojo.setPhotoUrl(itemsDTO.getPhotoUrl());
        itemPojo.setPrice(itemsDTO.getPrice());
        itemPojo.setCount(itemsDTO.getCount());
        return itemPojo;
    }

    public static List<ItemsDTO> convertListItems(List<ItemPojo> itemPojoList) {
        List<ItemsDTO> items = new ArrayList<>();
        for (ItemPojo itemPojo : itemPojoList) {
            items.add(Converter.convertItem(itemPojo));
        }
        return items;
    }


}
