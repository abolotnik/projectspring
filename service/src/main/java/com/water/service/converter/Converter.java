package com.water.service.converter;


import com.water.repository.model.*;
import com.water.repository.util.Role;
import com.water.service.model.*;
import org.apache.log4j.Logger;

import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Converter {

    private static final Logger logger = Logger.getLogger(Converter.class);
    private static Files uriRef;

    public static UserPojo convert(UserDTO userDTO) {
        logger.info("Convert DTO to POJO");
        UserPojo userPojo = UserPojo.getInstance();

        userPojo.setEmail((userDTO.getEmail() != null) ? userDTO.getEmail() : null);
        userPojo.setName((userDTO.getName() != null) ? userDTO.getName() : null);
        userPojo.setSurname((userDTO.getSurname() != null) ? userDTO.getSurname() : null);
        userPojo.setRole(Role.ROLE_USER.name());

        return userPojo;
    }

    public static UserDTO convert(UserPojo userPojo) {

        logger.info("Convert POJO to DTO");
        UserDTO userDTO = new UserDTO();

        userDTO.setEmail(userPojo.getEmail() != null ? userPojo.getEmail() : null);
        userDTO.setName(userPojo.getName() != null ? userPojo.getName() : null);
        userDTO.setSurname(userPojo.getSurname() != null ? userPojo.getSurname() : null);
        userDTO.setPassword(userPojo.getPassword() != null ? userPojo.getPassword() : null);
        userDTO.setRole(userPojo.getRole());

        return userDTO;
    }

    public static UserDTO convertUser(UserPojo userPojo) {
        UserDTO user = new UserDTO();

        user.setEmail(userPojo.getEmail());
        user.setName(userPojo.getName());
        user.setPassword(userPojo.getPassword());
        user.setRole(Role.ROLE_USER.name());

        return user;
    }

    public static UserDTO convertAddress(AddressPojo addressPojo) {
        UserDTO user = new UserDTO();

        return user;
    }

    public static UserDTO convertPhones(UserPojo userPojo) {
        UserDTO user = new UserDTO();

        return user;
    }

    public static UserDTO convertOrders(UserPojo userPojo) {
        UserDTO user = new UserDTO();

        return user;
    }

    public static OrderDTO convertOrder(OrderPojo orderPojo) {
        OrderDTO order = new OrderDTO();

        order.setId(orderPojo.getId());
        order.setPriceOrder(orderPojo.getPriceOrder());
        order.setAddress(orderPojo.getAddress());
        order.setContact(orderPojo.getAddress());
        order.setStatus(order.getStatus());


        return order;
    }

    public static ItemsDTO convertItem(ItemPojo itemPojo) {
        ItemsDTO itemsDTO = ItemsDTO.newBuilder()
                .id(itemPojo.getId())
                .name(itemPojo.getName())
                .description(itemPojo.getDescription())
                .photoUrl(itemPojo.getPhotoUrl())
                .price(itemPojo.getPrice())
                .count(itemPojo.getCount())
                .build();
        return itemsDTO;
    }

    public static ItemPojo convertItem(ItemsDTO itemsDTO) {
        ItemPojo itemPojo = new ItemPojo();
        itemPojo.setName(itemsDTO.getName());
        itemPojo.setDescription(itemsDTO.getDescription());
        itemPojo.setPhotoUrl(itemsDTO.getPhotoUrl());
        itemPojo.setPrice(itemsDTO.getPrice());
        itemPojo.setCount(itemsDTO.getCount());
        return itemPojo;
    }

    public static BasketDTO convertItemToBasket (ItemsDTO itemsDTO){
        BasketDTO basketDTO = new BasketDTO();
        basketDTO.setId(itemsDTO.getId());
        basketDTO.setName(itemsDTO.getName());
        basketDTO.setPhotoUrl(itemsDTO.getPhotoUrl());
        basketDTO.setDescription(itemsDTO.getDescription());
        basketDTO.setCount(itemsDTO.getCount());
        basketDTO.setPrice((itemsDTO.getPrice()*itemsDTO.getCount()));
        return basketDTO;
    }



    public static BasketDTO convertBasket(BasketPojo basketPojo) {
        BasketDTO basketDTO = new BasketDTO();
        basketDTO.setId(basketPojo.getId());
        basketDTO.setName(basketPojo.getName());
        basketDTO.setPhotoUrl(basketPojo.getPhotoUrl());
        basketDTO.setDescription(basketPojo.getDescription());
        basketDTO.setPrice(basketPojo.getPrice());
        basketDTO.setCount(basketPojo.getCount());
        return basketDTO;
    }

    public static BasketPojo convertBasket(BasketDTO basketDTO) {
        BasketPojo basketPojo = new BasketPojo();
        basketPojo.setName(basketDTO.getName());
        basketPojo.setPhotoUrl(basketDTO.getPhotoUrl());
        basketPojo.setDescription(basketDTO.getDescription());
        basketPojo.setCount(basketDTO.getCount());
        basketPojo.setPrice(basketDTO.getPrice());
        basketPojo.setActive(true);
        return basketPojo;
    }

//    public static NewsDTO convertNews(NewsPojo newsPojo) {
//        NewsDTO news = NewsDTO.newBuilder()
//                .id(newsPojo.getId())
//                .title(newsPojo.getTitle())
//                .newsContent(newsPojo.getNewsContent())
//                .photoUrl(newsPojo.getPhotoUrl())
//                .build();
//
//        return news;
//
//    }

//    public static List<NewsDTO> convertListNews(List<NewsPojo> newsPojoSet) {
//        List<NewsDTO> news = new ArrayList<>();
//
//        for (NewsPojo newsPojo : newsPojoSet) {
//            news.add(Converter.convertNews(newsPojo));
//        }
//
//        return news;
//    }

    public static List<ItemsDTO> convertListItems(List<ItemPojo> itemPojoList) {
        List<ItemsDTO> items = new ArrayList<>();
        for (ItemPojo itemPojo : itemPojoList) {
            items.add(Converter.convertItem(itemPojo));
        }
        return items;
    }

    public static List<OrderDTO> convertListOrders(List<OrderPojo> orderPojoSet) {
        List<OrderDTO> orders = new ArrayList<>();

        for (OrderPojo orderPojo : orderPojoSet) {
            orders.add(Converter.convertOrder(orderPojo));
        }

        return orders;

    }





    public static List<UserDTO> convertListUsers(List<UserPojo> userPojoSet) {
        List<UserDTO> users = new ArrayList<>();

        for (UserPojo userPojo : userPojoSet) {
            users.add(Converter.convertUser(userPojo));
        }

        return users;

    }


}
