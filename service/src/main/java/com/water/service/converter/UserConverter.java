package com.water.service.converter;

import com.water.repository.model.AddressPojo;
import com.water.repository.model.PhonePojo;
import com.water.repository.model.UserPojo;
import com.water.repository.util.Role;
import com.water.service.model.AddressDTO;
import com.water.service.model.UserDTO;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


public class UserConverter {

    private static final Logger logger = Logger.getLogger(Converter.class);


    public static UserPojo convert(UserDTO userDTO) {
        logger.info("Convert USER DTO to POJO");
        UserPojo userPojo = UserPojo.getInstance();
        userPojo.setEmail((userDTO.getEmail() != null) ? userDTO.getEmail() : null);
        userPojo.setName((userDTO.getName() != null) ? userDTO.getName() : null);
        userPojo.setSurname((userDTO.getSurname() != null) ? userDTO.getSurname() : null);
        userPojo.setRole(Role.ROLE_USER.name());
        return userPojo;
    }

    public static UserDTO convert(UserPojo userPojo) {

        logger.info("Convert USER POJO to DTO");
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userPojo.getId() != null ? userPojo.getId() : null);
        userDTO.setEmail(userPojo.getEmail() != null ? userPojo.getEmail() : null);
        userDTO.setName(userPojo.getName() != null ? userPojo.getName() : null);
        userDTO.setSurname(userPojo.getSurname() != null ? userPojo.getSurname() : null);
        userDTO.setPassword(userPojo.getPassword() != null ? userPojo.getPassword() : null);
        userDTO.setRole(userPojo.getRole());
        userDTO.setStatus(userPojo.isStatus());
        return userDTO;
    }

    public static UserDTO convert(PhonePojo phonePojo) {

        logger.info("Convert USER POJO to DTO");
        UserDTO userDTO = new UserDTO();

        return userDTO;
    }

    public static UserDTO convertPhones(UserPojo userPojo) {
        UserDTO user = new UserDTO();
        return user;
    }

    public static UserDTO convertOrders(UserPojo userPojo) {
        UserDTO user = new UserDTO();

        return user;
    }

    public static AddressDTO convertAddress(AddressPojo addressPojo) {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setId(addressPojo.getId());
        addressDTO.setPostCode(addressPojo.getPostCode());
        addressDTO.setCountry(addressPojo.getCountry());
        addressDTO.setCity(addressPojo.getCity());
        addressDTO.setStreet(addressPojo.getStreet());
        addressDTO.setBuilding(addressPojo.getBuilding());
        addressDTO.setApartament(addressPojo.getApartament());
        return addressDTO;
    }

    public static AddressPojo convertAddress(AddressDTO addressDTO) {
        AddressPojo addressPojo = new AddressPojo();
        addressPojo.setPostCode(addressDTO.getPostCode());
        addressPojo.setCountry(addressDTO.getCountry());
        addressPojo.setCity(addressDTO.getCity());
        addressPojo.setStreet(addressDTO.getStreet());
        addressPojo.setBuilding(addressDTO.getBuilding());
        addressPojo.setApartament(addressDTO.getApartament());
        return  addressPojo;
    }

    public static List<UserDTO> convert(List<UserPojo> userPojoList){
        List<UserDTO> users = new ArrayList<>();
        for (UserPojo userPojo : userPojoList) {
            users.add(UserConverter.convert(userPojo));
        }
        return users;
    }


}
