package com.water.service.converter;


import com.water.repository.model.NewsPojo;
import com.water.service.model.NewsDTO;

import java.util.ArrayList;
import java.util.List;

public class NewsConverter {

    public static NewsDTO convertNews(NewsPojo newsPojo) {
        NewsDTO news = NewsDTO.newBuilder()
                .id(newsPojo.getId())
                .title(newsPojo.getTitle())
                .content(newsPojo.getNewsContent())
                .photoUrl(newsPojo.getPhotoUrl())
                .build();
        return news;

    }

    public static NewsPojo convertNews(NewsDTO newsDTO) {
        NewsPojo newsPojo = new NewsPojo();
        newsPojo.setTitle(newsDTO.getTitle());
        newsPojo.setPhotoUrl(newsDTO.getPhotoUrl());
        newsPojo.setNewsContent(newsDTO.getContent());
        return newsPojo;

    }

    public static List<NewsDTO> convertListNews(List<NewsPojo> newsPojoList) {
        List<NewsDTO> news = new ArrayList<>();
        for (NewsPojo newsPojo : newsPojoList) {
            news.add(NewsConverter.convertNews(newsPojo));
        }
        return news;
    }

}
