package com.water.service.converter;


import com.water.repository.model.OrderPojo;
import com.water.repository.util.Status;
import com.water.service.model.OrderDTO;

public class OrderConverter {

    public static OrderPojo convertListItems(OrderDTO orderDTO) {
        OrderPojo orderPojo = new OrderPojo();
        orderPojo.setStatus(Status.NEW.name());
        orderPojo.setComment(orderDTO.getComment());
        orderPojo.setBaskets(orderDTO.getBaskets());


        return orderPojo;
    }


}
