package com.water.service.model;
import org.springframework.web.multipart.MultipartFile;


public class NewsDTO {

    private Integer id;
    private String title;
    private String photoUrl;
    private String content;
    private MultipartFile file;

    public NewsDTO() {
    }

    private NewsDTO(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setPhotoUrl(builder.photoUrl);
        setContent(builder.content);
        file = builder.file;
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static final class Builder {
        private Integer id;
        private String title;
        private String photoUrl;
        private String content;
        private MultipartFile file;

        private Builder() {
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }

        public Builder title(String val) {
            title = val;
            return this;
        }

        public Builder photoUrl(String val) {
            photoUrl = val;
            return this;
        }

        public Builder content(String val) {
            content = val;
            return this;
        }

        public Builder file(MultipartFile val) {
            file = val;
            return this;
        }

        public NewsDTO build() {
            return new NewsDTO(this);
        }
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
