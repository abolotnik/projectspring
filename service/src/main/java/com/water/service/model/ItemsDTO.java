package com.water.service.model;


public class ItemsDTO {


    private Integer id;
    private String name;
    private String photoUrl;
    private String description;
    private Double price;
    private Integer count;

    public ItemsDTO() {
    }

    private ItemsDTO(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setPhotoUrl(builder.photoUrl);
        setDescription(builder.description);
        setPrice(builder.price);
        setCount(builder.count);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {
        return price;
    }



    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public static final class Builder {
        private Integer id;
        private String name;
        private String photoUrl;
        private String description;
        private Double price;
        private Integer count;

        private Builder() {
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder photoUrl(String val) {
            photoUrl = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder price(Double val) {
            price = val;
            return this;
        }

        public Builder count(Integer val) {
            count = val;
            return this;
        }

        public ItemsDTO build() {
            return new ItemsDTO(this);
        }
    }

}
