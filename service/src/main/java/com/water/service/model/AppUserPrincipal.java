package com.water.service.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import com.water.repository.model.UserPojo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


public class AppUserPrincipal implements UserDetails {


    private static final long serialVersionUID = -3278431039657353380L;

    private UserPojo user;

    private Collection<SimpleGrantedAuthority> grantedAuthorities;


    public AppUserPrincipal(UserPojo user) {
        this.user = user;
        grantedAuthorities = new ArrayList<>();
        grantedAuthorities.addAll(Collections.singletonList(
                new SimpleGrantedAuthority(user.getRole())
        ));
    }

    public Integer getUserId() {
        return user.getId();
    }

    public String getRole() {
        return user.getRole();
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return user.isStatus();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
