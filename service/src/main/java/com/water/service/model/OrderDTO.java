package com.water.service.model;


import com.water.repository.model.AddressPojo;
import com.water.repository.model.BasketPojo;
import com.water.repository.model.UserPojo;

import java.util.List;

public class OrderDTO {




    private Integer id;
    private Double priceOrder;
    private String status;
    private UserPojo user;
    private AddressPojo address;
    private AddressPojo contact;
    private String comment;
    private List<BasketPojo> baskets;

    public OrderDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPriceOrder() {
        return priceOrder;
    }

    public void setPriceOrder(Double priceOrder) {
        this.priceOrder = priceOrder;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserPojo getUser() {
        return user;
    }

    public void setUser(UserPojo user) {
        this.user = user;
    }

    public AddressPojo getAddress() {
        return address;
    }

    public void setAddress(AddressPojo address) {
        this.address = address;
    }

    public AddressPojo getContact() {
        return contact;
    }

    public void setContact(AddressPojo contact) {
        this.contact = contact;
    }

    public List<BasketPojo> getBaskets() {
        return baskets;
    }

    public void setBaskets(List<BasketPojo> baskets) {
        this.baskets = baskets;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
