package com.water.web.controller;

import com.water.service.model.UserDTO;
import com.water.service.interfaces.UserInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegisterController {

    private final UserInterface userInterface;

    @Autowired
    public RegisterController(UserInterface userInterface) {
        this.userInterface = userInterface;
    }

    @RequestMapping(value = {"/registration"}, method = RequestMethod.GET)
    public String showRegistrationPage(Model model,
                                       @ModelAttribute("user") UserDTO user) {
        return "registration";
    }


    @RequestMapping(value = {"/registration"}, method = RequestMethod.POST)
    public String registerUser(Model model,
                               @ModelAttribute("user") UserDTO user,
                               BindingResult result) {
        if (!result.hasErrors()) {
            if (userInterface.checkUserByEmail(user.getEmail(), user.getPassword())) {
                userInterface.saveUser(user);
                model.addAttribute("message", "User "+user.getName()+" was registred");
                model.addAttribute("user", user);
                return "redirect:/login";
            }else {
                model.addAttribute("error", "User with this Email is exist");
                return "redirect:/registration";
            }
        } else {
            return "redirect:/registration";
        }
    }
}
