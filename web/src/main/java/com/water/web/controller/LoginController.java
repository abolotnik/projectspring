package com.water.web.controller;

import com.water.service.interfaces.UserInterface;
import com.water.service.model.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    private final UserInterface userInterface;
    private static final Logger log = LoggerFactory.getLogger("application");

    @Autowired
    public LoginController(UserInterface userInterface) {
        this.userInterface = userInterface;
    }


    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public String showLoginPage(@ModelAttribute("user") UserDTO user) {
        log.info("Login page show");
        return "login";
    }



}
