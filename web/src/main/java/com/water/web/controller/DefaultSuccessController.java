package com.water.web.controller;

import com.water.repository.util.Role;
import com.water.service.model.AppUserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DefaultSuccessController {

    private static final Logger log = LoggerFactory.getLogger("DefaultSuccessController");

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public String filterDirectedUrl() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if (principal.getRole().equalsIgnoreCase(Role.ROLE_USER.name())) {
            log.info("Role like USER");
            return "redirect:/user/news";
        } else if (principal.getRole().equalsIgnoreCase(Role.ROLE_ADMIN.name())) {
            log.info("Role like ADMIN");
            return "redirect:/admin/news";
        } else if (principal.getRole().equalsIgnoreCase(Role.ROLE_SUPERADMIN.name())) {
            log.info("Role like SUPERADMIN");
            return "redirect:/admin/news";
        }
        {
            return "redirect:/login";

        }
    }
}

