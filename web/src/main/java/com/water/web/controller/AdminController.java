package com.water.web.controller;

import com.water.service.interfaces.NewsInterface;
import com.water.service.model.NewsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Properties;
import java.util.Set;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    private static final Logger log = LoggerFactory.getLogger("AdminController");
    private final NewsInterface newsInterface;
    private final Properties properties;

    @Autowired
    public AdminController(NewsInterface newsInterface, Properties properties) {
        this.newsInterface = newsInterface;
        this.properties = properties;
    }


    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String showNewsPageForAdmin(Model model) {
        List<NewsDTO> news = newsInterface.getAllNews();
        model.addAttribute("news", news);
        return "admin/news";
    }

    @RequestMapping(value = "/news/add", method = RequestMethod.GET)
    public String addNews(Model model,
                          @ModelAttribute("news") NewsDTO news,
                          BindingResult result) {
        return "admin/news_add";
    }


}
