package com.water.web.controller.user;

import com.water.service.converter.Converter;
import com.water.service.interfaces.BasketInterface;
import com.water.service.interfaces.ItemInterface;
import com.water.service.model.ItemsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Properties;

@Controller
@RequestMapping(value = "/user")
public class UserCatalogController {


    private final ItemInterface itemInterface;
    private final BasketInterface basketInterface;
    private final Properties properties;

    @Autowired
    public UserCatalogController(ItemInterface itemInterface, BasketInterface basketInterface, Properties properties) {
        this.itemInterface = itemInterface;
        this.basketInterface = basketInterface;
        this.properties = properties;
    }


    @RequestMapping(value = {"/catalog"}, method = RequestMethod.GET)
    public String showAllItems(Model model,
                                    @ModelAttribute("item") ItemsDTO item
    ) {
        List<ItemsDTO> catalog = itemInterface.getCatalog();
        model.addAttribute("catalog", catalog);
        return "user/catalog";
    }


    @RequestMapping(value = "/catalog/add_item/{id}", method = RequestMethod.POST)
    public String addNewItemToBasket(Model model,
                                     @PathVariable Integer id,
                                     @ModelAttribute("item") ItemsDTO item
    ) {
        Integer count = item.getCount();
        item = itemInterface.getById(id);
        item.setCount(count);
        basketInterface.addItemToBasket(Converter.convertItemToBasket(item));
        return "redirect:/user/catalog";
    }

}
