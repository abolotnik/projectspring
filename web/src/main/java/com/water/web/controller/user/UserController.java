/*
package com.water.web.controller;

import com.water.repository.model.BasketPojo;
import com.water.service.converter.BasketConverter;
import com.water.service.converter.Converter;
import com.water.service.interfaces.BasketInterface;
import com.water.service.interfaces.ItemInterface;
import com.water.service.interfaces.NewsInterface;
import com.water.service.interfaces.UserInterface;
import com.water.service.model.BasketDTO;
import com.water.service.model.ItemsDTO;
import com.water.service.model.NewsDTO;
import com.water.service.model.UserDTO;
import com.water.web.impl.UserInterfaceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Properties;


@Controller
@RequestMapping(value = "/user")
public class UserController extends UserInterfaceImpl {

    private final UserInterface userInterface;
    private final NewsInterface newsInterface;
    private final ItemInterface itemInterface;
    private final BasketInterface basketInterface;
    private final Properties properties;

    @Autowired
    public UserController(
            UserInterface userInterface,
            NewsInterface newsInterface,
            ItemInterface itemInterface,
            BasketInterface basketInterface,
            Properties properties
    ) {
        this.userInterface = userInterface;
        this.newsInterface = newsInterface;
        this.itemInterface = itemInterface;
        this.basketInterface = basketInterface;
        this.properties = properties;
    }


    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String showNewsPageForAdmin(Model model) {
        List<NewsDTO> news = newsInterface.getAllNews();
        model.addAttribute("news", news);
        model.addAttribute("location", properties.getProperty("photo.url"));
        return "user/news";
    }






}
*/
