//package com.water.web.controller.user;
//
//import com.water.service.interfaces.BasketInterface;
//import com.water.service.interfaces.ItemInterface;
//import com.water.service.model.BasketDTO;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import java.util.List;
//import java.util.Properties;
//
//@Controller
//public class UserOrderController {
//
//    private final ItemInterface itemInterface;
//    private final BasketInterface basketInterface;
//    private final Properties properties;
//
//    public UserOrderController(ItemInterface itemInterface, BasketInterface basketInterface, Properties properties) {
//        this.itemInterface = itemInterface;
//        this.basketInterface = basketInterface;
//        this.properties = properties;
//    }
//
//
//    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
//    public String getUsersBasket(Model model,
//                                 @ModelAttribute("basket") BasketDTO basket
//    ) {
//        List<BasketDTO> baskets = basketInterface.getBasket();
//        Double totalPrice = 0d;
//        for (BasketDTO basketDTO : baskets) {
//            totalPrice += basketDTO.getPrice();
//        }
//        model.addAttribute("baskets", baskets);
//        model.addAttribute("totalPrice", totalPrice);
//        return "user/basket";
//    }
//
//}
