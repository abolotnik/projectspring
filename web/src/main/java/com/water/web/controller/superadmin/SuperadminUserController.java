package com.water.web.controller.superadmin;

import com.water.repository.util.Role;
import com.water.service.interfaces.UserInterface;
import com.water.service.model.UserDTO;
import com.water.web.validator.UserValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/superadmin")
public class SuperadminUserController {

    private static final Logger logger = Logger.getLogger(SuperadminUserController.class);
    private final UserInterface userInterface;
    private final UserValidator userValidator;

    @Autowired
    public SuperadminUserController(UserInterface userInterface, UserValidator userValidator) {
        this.userInterface = userInterface;
        this.userValidator = userValidator;
    }


    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String showUserForAdmin(Model model) {
        List<UserDTO> users = userInterface.getAllUser();
        model.addAttribute("users", users);
        return "superadmin/users";
    }

    @RequestMapping(value = {"/users/delete/{id}"}, method = RequestMethod.GET)
    public String deleteItemFromBasket(Model model,
                                       @PathVariable Integer id
    ) {
        try {
            userInterface.deleteUserById(id);
            model.addAttribute("message", "User delete successfull");
        } catch (Exception e) {
            model.addAttribute("message", "User not might been delete");
        }
        showUserForAdmin(model);
        return "superadmin/users";
    }


    @RequestMapping(value = "/users/pass/edit/{id}", method = RequestMethod.GET)
    public String editPassGet(Model model,
                              @PathVariable Integer id,
                              @ModelAttribute("user") UserDTO user,
                              BindingResult result) {
        user = userInterface.getUserById(id);
        model.addAttribute("user", user);
        return "superadmin/user_edit_pass";
    }

    @RequestMapping(value = "/users/pass/edit/{id}", method = RequestMethod.POST)
    public String editPassPost(Model model,
                               @PathVariable Integer id,
                               @ModelAttribute("user") UserDTO user,
                               BindingResult result) {
        userValidator.validate(user, result);
        if (!result.hasErrors()) {
            userInterface.changePassword(user);
            model.addAttribute("message", "User edit successfull");

            return "redirect:/superadmin/users/" + id;
        } else {
            model.addAttribute("message", "User edit unsuccessfull");
            return "redirect:/superadmin/users/" + id;
        }
    }


    @RequestMapping(value = "/users/role/edit/{id}", method = RequestMethod.GET)
    public String editRoleGet(Model model,
                              @PathVariable Integer id,
                              @ModelAttribute("user") UserDTO user,
                              BindingResult result) {
        user = userInterface.getUserById(id);
        List<String> roles = getRolesList(user);
        model.addAttribute("user", user);
        model.addAttribute("roles", roles);
        return "superadmin/user_edit_role";

    }

    @RequestMapping(value = "/users/role/edit/{id}", method = RequestMethod.POST)
    public String editRolePost(Model model,
                               @PathVariable Integer id,
                               @ModelAttribute("user") UserDTO user,
                               BindingResult result) {
        userValidator.validate(user, result);
        if (!result.hasErrors()) {
            userInterface.changeRole(user);
            model.addAttribute("message", "User edit successfull");
            return "redirect:/superadmin/users/" + id;
        } else {
            model.addAttribute("message", "User edit unsuccessfull");
            return "redirect:/superadmin/users/" + id;
        }
    }


    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public String getUserById(Model model,
                              @PathVariable Integer id,
                              @ModelAttribute("user") UserDTO user,
                              BindingResult result) {
        user = userInterface.getUserById(id);
        model.addAttribute("user", user);
        return "superadmin/user_show";
    }

    List<String> getRolesList(UserDTO userDTO) {
        List<String> roles = new ArrayList<>();

        if (!userDTO.getRole().equals(Role.ROLE_USER)) {
            roles.add(Role.ROLE_USER.name());
        }
        if (!userDTO.getRole().equals(Role.ROLE_ADMIN)) {
            roles.add(Role.ROLE_ADMIN.name());
        }
        if (!userDTO.getRole().equals(Role.ROLE_SUPERADMIN)) {
            roles.add(Role.ROLE_SUPERADMIN.name());
        }
        return roles;
    }
}
