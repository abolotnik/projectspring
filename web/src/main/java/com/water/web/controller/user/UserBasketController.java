package com.water.web.controller.user;

import com.water.service.interfaces.BasketInterface;
import com.water.service.model.BasketDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Properties;

@Controller
@RequestMapping(value = "/user")
public class UserBasketController {


    private final BasketInterface basketInterface;
    private final Properties properties;

    @Autowired
    public UserBasketController(BasketInterface basketInterface, Properties properties) {
        this.basketInterface = basketInterface;
        this.properties = properties;
    }


    @RequestMapping(value = {"/basket"}, method = RequestMethod.GET)
    public String getUsersBasket(Model model,
                                 @ModelAttribute("basket") BasketDTO basket
    ) {
        List<BasketDTO> baskets = basketInterface.getBasket();
        Double totalPrice = 0d;
        for (BasketDTO basketDTO : baskets) {
            totalPrice += basketDTO.getPrice();
        }
        model.addAttribute("baskets", baskets);
        model.addAttribute("totalPrice", totalPrice);
        return "user/basket";
    }

    @RequestMapping(value = {"/basket/delete_item/{id}"}, method = RequestMethod.POST)
    public String deleteItemFromBasket(Model model,
                                       @PathVariable Integer id,
                                       @ModelAttribute("basket") BasketDTO basket
    ) {
        basketInterface.deleteById(id);
        model.addAttribute("message", "Item delete successfull");
        return "redirect:/user/basket";
    }

    @RequestMapping(value = {"/basket/change_item/{id}"}, method = RequestMethod.GET)
    public String changeItemById(Model model,
                                 @PathVariable Integer id
    ) {
        return "redirect:/user/basket/{id}";
    }

    @RequestMapping(value = {"/basket/{id}"}, method = RequestMethod.GET)
    public String getItemFromBasketById(Model model,
                                        @PathVariable Integer id,
                                        @ModelAttribute("basket") BasketDTO basket
    ) {
        basket = basketInterface.getById(id);
        model.addAttribute("basket", basket);
        return "user/basket_id";
    }

    @RequestMapping(value = {"/basket/change_item/{id}"}, method = RequestMethod.POST)
    public String changeItemByIdSuccess(Model model,
                                        @PathVariable Integer id,
                                        @ModelAttribute("basket") BasketDTO basket,
                                        @ModelAttribute("message1") String message1
    ) {
        basketInterface.changeBasketById(basket);
        message1 = "s;jgh;skjf";
        model.addAttribute("message", "Item with id " + basket.getId() + " changed successfull");
        getUsersBasket(model, basket);
        return "redirect:/user/basket";
    }

}
