package com.water.web.controller.user;

import com.water.service.interfaces.NewsInterface;
import com.water.service.model.NewsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Properties;

@Controller
@RequestMapping(value = "/user")
public class UserNewsController {

    private final NewsInterface newsInterface;
    private final Properties properties;

    @Autowired
    public UserNewsController(NewsInterface newsInterface, Properties properties) {
        this.newsInterface = newsInterface;
        this.properties = properties;
    }


    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String showNewsPageForAdmin(Model model) {
        List<NewsDTO> news = newsInterface.getAllNews();
        model.addAttribute("news", news);
        model.addAttribute("location", properties.getProperty("photo.url"));
        return "user/news";
    }

}
