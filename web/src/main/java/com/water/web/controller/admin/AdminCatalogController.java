package com.water.web.controller.admin;

import com.water.service.interfaces.ItemInterface;
import com.water.service.model.ItemsDTO;
import com.water.web.validator.ItemValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Properties;

@Controller
@RequestMapping(value = "/admin")
public class AdminCatalogController {

    private final ItemInterface itemInterface;
    private final ItemValidator itemValidator;
    private final Properties properties;

    @Autowired
    public AdminCatalogController(ItemInterface itemInterface, ItemValidator itemValidator, Properties properties) {
        this.itemInterface = itemInterface;
        this.itemValidator = itemValidator;
        this.properties = properties;
    }


    @RequestMapping(value = "/catalog", method = RequestMethod.GET)
    public String showItemsForAdmin(Model model,
                                    @ModelAttribute("item") ItemsDTO item
    ) {
        List<ItemsDTO> items = itemInterface.getCatalog();
        model.addAttribute("items", items);
        return "admin/catalog";
    }

    @RequestMapping(value = "/catalog/add", method = RequestMethod.GET)
    public String addNews(
            @ModelAttribute("item") ItemsDTO item
    ) {
        return "admin/catalog_add";
    }

    @RequestMapping(value = "/catalog/add", method = RequestMethod.POST)
    public String addItemForSaving(Model model,
                                   @ModelAttribute("item") ItemsDTO item,
                                   BindingResult result) {
        try {
            itemValidator.validate(item, result);
        } catch (NullPointerException e) {
            model.addAttribute("message", "Please, check fields your inputs");
        }
        if (!result.hasErrors()) {
            itemInterface.addItem(item);
            return "redirect:/admin/catalog";
        } else {
            return "admin/catalog_add";
        }
    }

    @RequestMapping(value = {"/catalog/delete/{id}"}, method = RequestMethod.GET)
    public String deleteItemFromBasket(Model model,
                                       @PathVariable Integer id,
                                       @ModelAttribute("item") ItemsDTO item
    ) {
        try {
            itemInterface.deleteById(id);
            model.addAttribute("message", "Item delete successfull");
        } catch (Exception e) {
            model.addAttribute("message", "Item not might been delete");
        }
        showItemsForAdmin(model, item);
        return "admin/catalog";
    }

    @RequestMapping(value = "/catalog/edit/{id}", method = RequestMethod.GET)
    public String editNewsById(Model model,
                               @PathVariable Integer id,
                               @ModelAttribute("item") ItemsDTO item,
                               BindingResult result) {
        item = itemInterface.getById(id);
        model.addAttribute("item", item);
        return "admin/catalog_edit";
    }

    @RequestMapping(value = "/catalog/edit/{id}", method = RequestMethod.POST)
    public String editNews(Model model,
                           @PathVariable Integer id,
                           @ModelAttribute("item") ItemsDTO item,
                           BindingResult result) {

        itemValidator.validate(item, result);
        if (!result.hasErrors()) {
            itemInterface.editItem(item);
            model.addAttribute("message", "Item edit successfull");
            showItemsForAdmin(model, item);
            return "admin/catalog";
        } else {
            return "admin/catalog_edit";
        }
    }

}
