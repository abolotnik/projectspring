package com.water.web.controller;

import com.water.repository.model.BasketPojo;
import com.water.service.converter.BasketConverter;
import com.water.service.converter.Converter;
import com.water.service.interfaces.BasketInterface;
import com.water.service.interfaces.ItemInterface;
import com.water.service.interfaces.NewsInterface;
import com.water.service.interfaces.UserInterface;
import com.water.service.model.BasketDTO;
import com.water.service.model.ItemsDTO;
import com.water.service.model.NewsDTO;
import com.water.service.model.UserDTO;
import com.water.web.impl.UserInterfaceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Properties;


@Controller
@RequestMapping(value = "/user")
public class UserController extends UserInterfaceImpl {

    private final UserInterface userInterface;
    private final NewsInterface newsInterface;
    private final ItemInterface itemInterface;
    private final BasketInterface basketInterface;
    private final Properties properties;

    @Autowired
    public UserController(
            UserInterface userInterface,
            NewsInterface newsInterface,
            ItemInterface itemInterface,
            BasketInterface basketInterface,
            Properties properties
    ) {
        this.userInterface = userInterface;
        this.newsInterface = newsInterface;
        this.itemInterface = itemInterface;
        this.basketInterface = basketInterface;
        this.properties = properties;
    }


    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String showNewsPageForAdmin(Model model) {
        List<NewsDTO> news = newsInterface.getAllNews();
        model.addAttribute("news", news);
        model.addAttribute("location", properties.getProperty("photo.url"));
        return "user/news";
    }

    @RequestMapping(value = {"/catalog"}, method = RequestMethod.GET)
    public String showRegistratPage(Model model,
                                    @ModelAttribute("item") ItemsDTO item
    ) {
        List<ItemsDTO> catalog = itemInterface.getCatalog();
        model.addAttribute("catalog", catalog);
        return "user/catalog";
    }


    @RequestMapping(value = "/catalog/add_item/{id}", method = RequestMethod.POST)
    public String addNewItemToBasket(Model model,
                                     @PathVariable Integer id,
                                     @ModelAttribute("item") ItemsDTO item
    ) {
        Integer count = item.getCount();
        item = itemInterface.getById(id);
        item.setCount(count);
        basketInterface.addItemToBasket(Converter.convertItemToBasket(item));
        return "redirect:/user/catalog";
    }

    @RequestMapping(value = {"/basket"}, method = RequestMethod.GET)
    public String getUsersBasket(Model model,
                                 @ModelAttribute("basket") BasketDTO basket
    ) {
        List<BasketDTO> baskets = basketInterface.getBasket();
        Double totalPrice = 0d;
        for (BasketDTO basketDTO : baskets) {
            totalPrice += basketDTO.getPrice();
        }
        model.addAttribute("baskets", baskets);
        model.addAttribute("totalPrice", totalPrice);
        return "user/basket";
    }

    @RequestMapping(value = {"/basket/delete_item/{id}"}, method = RequestMethod.POST)
    public String deleteItemFromBasket(Model model,
                                     @PathVariable Integer id,
                                     @ModelAttribute("basket") BasketDTO basket
    ) {
        basketInterface.deleteById(id);
        return "redirect:/user/basket";
    }

    @RequestMapping(value = {"/basket/change_item/{id}"}, method = RequestMethod.GET)
    public String changeItemById(Model model,
                                       @PathVariable Integer id
    ) {
        return "redirect:/user/basket/{id}";
    }

    @RequestMapping(value = {"/basket/{id}"}, method = RequestMethod.GET)
    public String getItemFromBasketById(Model model,
                                        @PathVariable Integer id,
                                        @ModelAttribute("basket") BasketDTO basket
    ) {
        basket = basketInterface.getById(id);
        model.addAttribute("basket", basket);
        return "user/basket_id";
    }

    @RequestMapping(value = {"/basket/change_item/{id}"}, method = RequestMethod.POST)
    public String changeItemByIdSuccess(Model model,
                                 @PathVariable Integer id,
                                 @ModelAttribute("basket") BasketDTO basket
    ) {
        Integer count = basket.getCount();
        Integer basketId = basket.getId();
//        ItemsDTO item = itemInterface.getById(basket.getItemId());
//        basket.setCount(count);
//        basket.setPrice(item.getPrice()*count);
//        item.setCount(count);
        basketInterface.changeBasketById(basketId, count);
//        basketInterface.addItemToBasket(Converter.convertItemToBasket(item));



//        basket.setCount(count);
//        basket.setPrice((itemsDTO.getPrice()*basket.getCount()));
//        basketInterface.addItemToBasket(basket);
        return "redirect:/user/basket";
    }




}
