package com.water.web.controller.admin;

import com.water.service.interfaces.NewsInterface;
import com.water.service.model.NewsDTO;
import com.water.web.validator.NewsValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Properties;

@Controller
//@MultipartConfig
@RequestMapping(value = "/admin")
public class AdminNewsController {


    private static final Logger log = LoggerFactory.getLogger("AdminController");
    private final NewsInterface newsInterface;
    private final NewsValidator newsValidator;
    private final Properties properties;

    @Autowired
    public AdminNewsController(NewsInterface newsInterface, NewsValidator newsValidator, Properties properties) {
        this.newsInterface = newsInterface;
        this.newsValidator = newsValidator;
        this.properties = properties;
    }


    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String showNewsPageForAdmin(Model model) {
        List<NewsDTO> news = newsInterface.getAllNews();
        model.addAttribute("news", news);
        return "admin/news";
    }


    @RequestMapping(value = "/news/add", method = RequestMethod.GET)
    public String addNews(
            @ModelAttribute("news") NewsDTO news
//            @RequestParam("file")MultipartFile file
    ) {
        return "admin/news_add";
    }

    @RequestMapping(value = "/news/add", method = RequestMethod.POST)
    public String addNewsForSaving(
            @ModelAttribute("news") NewsDTO news,
//            @RequestParam("file")MultipartFile file,
            BindingResult result) {
        newsValidator.validate(news, result);
        if (!result.hasErrors()) {
            newsInterface.addNews(news);
            return "redirect:/admin/news";
        } else {
            return "admin/news_add";
        }
    }

    @RequestMapping(value = {"/news/delete/{id}"}, method = RequestMethod.GET)
    public String deleteItemFromBasket(Model model,
                                       @PathVariable Integer id,
                                       @ModelAttribute("news") NewsDTO news
    ) {
        newsInterface.deleteById(id);
        model.addAttribute("message", "Item delete successfull");
        model.addAttribute("message", "News edit successfull");
        showNewsPageForAdmin(model);
        return "admin/news";
    }

    @RequestMapping(value = "/news/edit/{id}", method = RequestMethod.GET)
    public String editNewsById(Model model,
                               @PathVariable Integer id,
                               @ModelAttribute("news") NewsDTO news,
//            @RequestParam("file")MultipartFile file,
                               BindingResult result) {
        NewsDTO newsDTO = newsInterface.getNewsById(id);
        model.addAttribute("news", newsDTO);
        return "admin/news_edit";
    }

    @RequestMapping(value = "/news/edit/{id}", method = RequestMethod.POST)
    public String editNews(Model model,
                           @PathVariable Integer id,
                           @ModelAttribute("news") NewsDTO news,
//            @RequestParam("file")MultipartFile file,
                           BindingResult result) {
        newsValidator.validate(news, result);
        if (!result.hasErrors()) {
            newsInterface.editNews(news);
            model.addAttribute("message", "News edit successfull");
            showNewsPageForAdmin(model);
            return "admin/news";
        } else {
            return "admin/news_add";
        }
    }

}
