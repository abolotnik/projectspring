package com.water.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({
        "com.water.repository",
        "com.water.service",
        "com.water.web"})
public class ApplicationConfig {
}
