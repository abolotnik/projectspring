package com.water.web.validator;


import com.water.service.model.NewsDTO;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Service
public class NewsValidator implements Validator {



    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(NewsDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        NewsDTO newsDTO = (NewsDTO) o;
        ValidationUtils.rejectIfEmpty(errors, "title", "error.title.empty");
        ValidationUtils.rejectIfEmpty(errors, "content", "error.content.empty");
        ValidationUtils.rejectIfEmpty(errors, "photoUrl", "error.photoUrl.empty");
        if (newsDTO.getTitle().length() < 5) {
            errors.rejectValue("title", "error.title.length");
        }
        if (newsDTO.getContent().length() < 5) {
            errors.rejectValue("content", "error.content.length");
        }
        if (newsDTO.getPhotoUrl().length() < 3) {
            errors.rejectValue("photoUrl", "error.photoUrl.length");
        }
    }
}
