//package com.water.web.validator;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.validation.Errors;
//import org.springframework.validation.Validator;
//
//@Component
//public class RegisterValidator implements Validator {
//
//
//    @Autowired
//    private UserService userService;
//
//    @Override
//    public boolean supports(Class<?> clazz) {
//        return clazz.equals(UserDTO.class);
//    }
//
//    @Override
//    public void validate(Object o, Errors errors) {
//        UserDTO userDTO = (UserDTO) o;
//        ValidationUtils.rejectIfEmpty(errors, "email", "email_empty");
//        ValidationUtils.rejectIfEmpty(errors, "password", "password_empty");
//
//        if (userDTO.getPassword().length() < 5) {
//            errors.rejectValue("password", "password_length");
//        }
//
//        Pattern pattern = Pattern.compile("^\\w+@\\w+\\.\\w{2,3}$");
//        Matcher matcher = pattern.matcher(userDTO.getEmail());
//
//        if (!matcher.find()) {
//            errors.rejectValue("email", "email_incorrect");
//        }
//
//        pattern = Pattern.compile("\\w{5,}");
//        matcher = pattern.matcher(userDTO.getPassword());
//
//        if (!matcher.find()) {
//            errors.rejectValue("password", "password_length");
//        }
//
//        if (userService.getUserDTObyEmail(userDTO.getEmail()) != null) {
//            errors.rejectValue("email", "email_exist");
//        }
//    }
//
//
//
//}
