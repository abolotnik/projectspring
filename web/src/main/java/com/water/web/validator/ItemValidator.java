package com.water.web.validator;


import com.water.service.model.ItemsDTO;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Service
public class ItemValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(ItemsDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) throws NullPointerException{
        ItemsDTO itemsDTO = (ItemsDTO) o;
        ValidationUtils.rejectIfEmpty(errors, "name", "error.name.empty");
        ValidationUtils.rejectIfEmpty(errors, "description", "error.description.empty");
        ValidationUtils.rejectIfEmpty(errors, "photoUrl", "error.photoUrl.empty");
        ValidationUtils.rejectIfEmpty(errors, "count", "error.count.empty");
        ValidationUtils.rejectIfEmpty(errors, "price", "error.price.empty");
        if (itemsDTO.getCount().getClass().getName().equalsIgnoreCase("integer") ||
                itemsDTO.getCount().getClass().getName().equalsIgnoreCase("double") ||
                itemsDTO.getCount().getClass().getName().equalsIgnoreCase("float")
                ) {
            errors.rejectValue("count", "error.count.type");
        }

        if (itemsDTO.getPrice().getClass().getName().equalsIgnoreCase("integer") ||
                itemsDTO.getCount().getClass().getName().equalsIgnoreCase("double") ||
                itemsDTO.getCount().getClass().getName().equalsIgnoreCase("float")
                ) {
            errors.rejectValue("price", "error.price.type");
        }

        if (itemsDTO.getName().length() < 5) {
            errors.rejectValue("name", "error.name.length");
        }
        if (itemsDTO.getCount() < 0) {
            errors.rejectValue("count", "error.count.length");
        }
        if (itemsDTO.getDescription().length() < 5) {
            errors.rejectValue("description", "error.description.length");
        }
        if (itemsDTO.getPrice() < 0) {
            errors.rejectValue("price", "error.price.length");
        }
    }
}
