package com.water.web.impl;

import com.water.service.model.OrderDTO;
import com.water.web.interfaces.CatalogInterface;


public class CatalogInterfaceImpl implements CatalogInterface {
    @Override
    public Integer addNewItem() {

        return null;
    }

    @Override
    public OrderDTO changeItem(Integer itemId) {

        return null;
    }

    @Override
    public void addItemToBasket(Integer itemId) {

    }

    @Override
    public void deleteItemFromBasket(Integer itemId) {

    }

    @Override
    public void createNewOrder(OrderDTO order) {

    }

    @Override
    public void changeOrderForUser(Integer orderId) {

    }

    @Override
    public void changeOrderForAdmin(Integer orderId) {

    }
}
