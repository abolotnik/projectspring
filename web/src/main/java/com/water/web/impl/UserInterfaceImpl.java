package com.water.web.impl;

import com.water.service.model.UserDTO;
import com.water.web.interfaces.UserInterface;

import java.util.List;


public class UserInterfaceImpl implements UserInterface {


    UserInterface userInterface;


    @Override
    public UserDTO getUserByEmail(String email) {
        UserDTO user = new UserDTO();
        return user;
    }

    @Override
    public void addNewUser(UserDTO user) {

    }

    @Override
    public void deactivateUser(Integer userId) {

    }

    @Override
    public void activateUser(Integer userId) {

    }

    @Override
    public void deleteUser(Integer userId) {

    }

    @Override
    public List<UserDTO> getListUsers() {
        return null;
    }

    @Override
    public void addAddressForUserById(Integer id) {

    }
}
