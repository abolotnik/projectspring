package com.water.web.interfaces;


import com.water.service.model.UserDTO;

import java.util.List;


public interface UserInterface {

    UserDTO getUserByEmail(String email);

    void addNewUser(UserDTO user);

    void deactivateUser(Integer userId);

    void activateUser(Integer userId);

    void deleteUser(Integer userId);

    List<UserDTO> getListUsers();

    void addAddressForUserById(Integer id);





}
