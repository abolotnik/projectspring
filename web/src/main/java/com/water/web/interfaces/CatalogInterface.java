package com.water.web.interfaces;


import com.water.service.model.OrderDTO;

public interface CatalogInterface {


    public Integer addNewItem();

    public OrderDTO changeItem(Integer itemId);

    public void addItemToBasket(Integer itemId);

    public void deleteItemFromBasket(Integer itemId);

    public void createNewOrder(OrderDTO order);

    public void changeOrderForUser(Integer orderId);

    public void changeOrderForAdmin(Integer orderId);






}
