CREATE DATABASE if NOT EXISTS onlinemarket;
USE onlinemarket;

DROP TABLE IF EXISTS T_NEWS;
DROP TABLE IF EXISTS T_PHONE;
DROP TABLE IF EXISTS T_ORDER_TRANSP;
DROP TABLE IF EXISTS T_BASKET;
DROP TABLE IF EXISTS T_ORDER;
DROP TABLE IF EXISTS T_USER_CONTACT;
DROP TABLE IF EXISTS T_ADDRESS;
DROP TABLE IF EXISTS T_ITEM;
DROP TABLE IF EXISTS T_USER;


CREATE TABLE IF NOT EXISTS T_USER (
  F_ID       INT(5) AUTO_INCREMENT NOT NULL,
  F_NAME     VARCHAR(50)           NOT NULL,
  F_SURNAME  VARCHAR(50)           NOT NULL,
  F_EMAIL    VARCHAR(50)           NOT NULL,
  F_PASSWORD VARCHAR(255)          NOT NULL,
  F_ROLE     VARCHAR(20)           NOT NULL,
  IS_ACTIVE  BOOLEAN DEFAULT 1,

  PRIMARY KEY (F_ID)
);

CREATE TABLE IF NOT EXISTS T_ITEM (
  F_ID          INT(5) AUTO_INCREMENT NOT NULL,
  F_NAME        VARCHAR(50)           NOT NULL,
  F_PHOTO_URL   VARCHAR(255)          NOT NULL,
  F_DESCRIPTION VARCHAR(50)           NOT NULL,
  F_PRICE       DOUBLE(5, 2)          NOT NULL,
  F_COUNT       INT(2),
  PRIMARY KEY (F_ID)
);

CREATE TABLE IF NOT EXISTS T_PHONE (
  F_ID      INT(5) AUTO_INCREMENT NOT NULL,
  F_USER_ID INT(5)                NOT NULL,
  F_PHONE   VARCHAR(20)           NOT NULL,
  IS_ACTIVE BOOLEAN DEFAULT 1,
  PRIMARY KEY (F_ID),
  CONSTRAINT USER_PHONE FOREIGN KEY (F_USER_ID) REFERENCES T_USER (F_ID)
);

CREATE TABLE IF NOT EXISTS T_ADDRESS (
  F_ID         INT(5) AUTO_INCREMENT NOT NULL,
  F_USER_ID    INT(5)                NOT NULL,
  F_ORDER_ID    INT(5)                NOT NULL,
  F_POST_CODE  INT(6)                NOT NULL,
  F_COUNTRY    VARCHAR(255)          NOT NULL,
  F_CITY       VARCHAR(50)           NOT NULL,
  F_STREET     VARCHAR(255)          NOT NULL,
  F_BUILDING   INT(5)                NOT NULL,
  F_APARTAMENT INT(5)                NOT NULL,
  PRIMARY KEY (F_ID),
  CONSTRAINT USER_ADDRESS FOREIGN KEY (F_USER_ID) REFERENCES T_USER (F_ID)
);

CREATE TABLE IF NOT EXISTS T_USER_CONTACT (
  F_ID      INT(5) AUTO_INCREMENT NOT NULL,
  F_USER_ID INT(5)                NOT NULL,
  F_NAME    VARCHAR(255)          NOT NULL,
  F_SURNAME VARCHAR(255)          NOT NULL,
  F_PHONE   VARCHAR(20)           NOT NULL,
  PRIMARY KEY (F_ID),
  CONSTRAINT USER_CONTACT FOREIGN KEY (F_USER_ID) REFERENCES T_USER (F_ID)
);

CREATE TABLE IF NOT EXISTS T_BASKET (
  F_ID      INT(5) AUTO_INCREMENT NOT NULL,
  F_USER_ID INT(5)                NOT NULL,
  F_ITEM_ID INT(5)                NOT NULL,
  PRIMARY KEY (F_ID),
  CONSTRAINT BASKET_USER FOREIGN KEY (F_USER_ID) REFERENCES T_USER (F_ID),
  CONSTRAINT BASKET_ITEM FOREIGN KEY (F_ITEM_ID) REFERENCES T_ITEM (F_ID)
);

CREATE TABLE IF NOT EXISTS T_ORDER (
  F_ID          INT(5) AUTO_INCREMENT NOT NULL,
  F_USER_ID     INT(5)                NOT NULL,
  F_ITEM_ID     INT(5)                NOT NULL,
  F_ADDRESS_ID  INT(5)                NOT NULL,
  F_CONTACT_ID  INT(5),
  F_PRICE_ORDER DOUBLE(5, 2)          NOT NULL,
  F_COMMENT     VARCHAR(255),
  F_STATUS      VARCHAR(30)           NOT NULL,
  IS_ACTIVE     BOOLEAN DEFAULT 1,
  PRIMARY KEY (F_ID),
  CONSTRAINT ORDER_USER FOREIGN KEY (F_USER_ID) REFERENCES T_USER (F_ID),
  CONSTRAINT ORDER_ITEM FOREIGN KEY (F_ITEM_ID) REFERENCES T_ITEM (F_ID),
  CONSTRAINT ORDER_ADDRESS FOREIGN KEY (F_ADDRESS_ID) REFERENCES T_ADDRESS (F_ID),
  CONSTRAINT ORDER_CONTACT FOREIGN KEY (F_CONTACT_ID) REFERENCES T_USER_CONTACT (F_ID)
);

CREATE TABLE IF NOT EXISTS T_ORDER_TRANSP (
  F_ID      INT(5) AUTO_INCREMENT NOT NULL,
  F_ORDER_ID INT(5)                NOT NULL,
  F_ITEM_ID INT(5)                NOT NULL,
  PRIMARY KEY (F_ID),
  CONSTRAINT ORDER_TRANSP FOREIGN KEY (F_ORDER_ID) REFERENCES T_ORDER (F_ID),
  CONSTRAINT ITEM_TRANSP FOREIGN KEY (F_ITEM_ID) REFERENCES T_ITEM (F_ID)
);

CREATE TABLE IF NOT EXISTS T_NEWS (
  F_ID        INT(5) AUTO_INCREMENT NOT NULL,
  F_USER_ID   INT(5)                NOT NULL,
  F_TITLE     VARCHAR(255)          NOT NULL,
  F_PHOTO_URL VARCHAR(255)          NOT NULL,
  F_CONTENT   VARCHAR(255)          NOT NULL,
  IS_ACTIVE   BOOLEAN DEFAULT 1,
  PRIMARY KEY (F_ID),
  CONSTRAINT USER_NEWS FOREIGN KEY (F_USER_ID) REFERENCES T_USER (F_ID)
);

SHOW TABLES;


INSERT INTO T_USER
(F_NAME, F_SURNAME, F_EMAIL, F_PASSWORD, F_ROLE)
VALUES
  ('admin', 'admin', 'admin@admin', '$2a$10$NYTnsZLKinjmKapVcPtvWOcSQkb2Fl5bCEIxavW0l6WLwvyOFRpBq', 'ROLE_ADMIN');


INSERT INTO T_NEWS
(F_USER_ID, F_TITLE, F_PHOTO_URL, F_CONTENT)
VALUES
  (1, 'title1', 'E:\opt\images\1.jpg', 'content1'),
  (1, 'title2', 'E:\opt\images\2.jpg', 'content2'),
  (1, 'title3', 'E:\opt\images\3.jpg', 'content3'),
  (1, 'title4', 'E:\opt\images\4.jpg', 'content4'),
  (1, 'title5', 'E:\opt\images\5.jpg', 'content5'),
  (1, 'title6', 'E:\opt\images\6.jpg', 'content6'),
  (1, 'title7', 'E:\opt\images\7.jpg', 'content7'),
  (1, 'title8', 'E:\opt\images\8.jpg', 'content8');

INSERT INTO T_ITEM
(F_NAME, F_PHOTO_URL, F_DESCRIPTION, F_PRICE, F_COUNT)
VALUES
  ('title1', 'E:\opt\images\1.jpg', 'content1', 25,1),
  ('title2', 'E:\opt\images\2.jpg', 'content2', 25,1),
  ('title3', 'E:\opt\images\3.jpg', 'content3', 25,1),
  ('title4', 'E:\opt\images\4.jpg', 'content4', 25,1),
  ('title5', 'E:\opt\images\5.jpg', 'content5', 25,1),
  ('title6', 'E:\opt\images\6.jpg', 'content6', 25,1),
  ('title7', 'E:\opt\images\7.jpg', 'content7', 25,1),
  ('title8', 'E:\opt\images\8.jpg', 'content8', 25,1);


