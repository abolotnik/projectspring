﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<html>
<head>
  <title>Online_Shop</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
  <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
     }
    
    
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>

  <body>

  <div class="container">   
  <div class="col-sm-1">
  
  
    </div>
    </div>
	
	
	</div>
  <div class="container">    
  <div class="row">
    <div class="col-sm-2">

    </div>
	<div class="col-sm-2">
     
    </div>
	
	
    <div class="col-sm-4">

    <form:form class="form-signin" modelAttribute="user" action="/login" method="post">
        <h2 class="form-signin-heading" >Please sign in</h2>
        <c:if test="not empty ${error}">
            <c:out value="${error}"/>
        </c:if>
        <label  class="sr-only">Email address</label>
        <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required autofocus></br>
        <label  class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required></br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form:form>
        <form  action="/registration" method="get">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Registration</button>
        </form>
<%--<input type="file" id="url" name="photoUrl" placeholder="Chose photo, please"/>--%>

    </div>
	
    <div class="col-sm-4"> 
      
    </div>

  </div>
</div><br>
  


  </body>
</html>