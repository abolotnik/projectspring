<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
  <title>My orders</title>
</head>
<body>
<h1>My orders</h1>
<table style="height: 466px; width: 1168px;">
  <tbody>
    <tr>
      <td style="width: 152px;">
      </td>
      <td style="width: 679px;" colspan="1" rowspan="3"
 valign="top">
      <table align="center">
        <tbody>
          <tr>
            <th>Mark</th>
            <th>Type</th>
            <th>Scope of bottle</th>
            <th>Count of bottle</th>
            <th></th>
          </tr>
          <tr>
            <td>
            <input list="mark">
			<datalist id="mark">
			</datalist>
			</td>
            <td>
            <input list="type"><datalist id="type"></datalist></td>
            <td>
            <input list="scope"><datalist id="scope"></datalist></td>
            <td>
            <input name="count" type="text"></td>
            <td>
            <input value="Add" type="submit"></td>
          </tr>
        </tbody>
      </table>
      </td>
      <td style="width: 317px;">
      <c:choose><c:when test="${login!=null}"><c:out
 value="Hi, ${login}"><br>
      </c:out></c:when></c:choose>
      <form action="logout"><input value="Logout"
 type="submit"></form>
      <othewise>
      <c:out value="Hi, Guest"></c:out></othewise>
      <form action="login"><c:if
 test="messagelogin!=null"><c:out ${messagelogin=""><br>
        </c:out>
        <input name="login" value="Login" type="text"><br>
        <c:if test="messagepassword!=null">
        <c:out ${messagepassword=""><br>
        </c:out>
        <input name="password" value="Password"
 type="text"><br>
        <br>
        <input value="Log in" type="submit">
        </c:if></c:if></form>
      </td>
    </tr>
    <tr>
      <td style="width: 152px;">
      <form action="myorder"><input value="My orders"
 type="submit"></form>
      </td>
      <td style="width: 317px;">
      </td>
    </tr>
    <tr>
      <td style="width: 152px;"></td>
      <td style="width: 317px;"></td>
    </tr>
  </tbody>
</table>
</body>
</html>
