﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Online_Shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>

<body>

<div class="container">
    <div class="col-sm-1">


    </div>
</div>


</div>
<div class="container">
    <div class="row">
        <div class="col-sm-2">

        </div>

        <div class="col-sm-2">

        </div>

        <%--Form input registration info--%>
        <div class="col-sm-4">
            <form:form method="post" modelAttribute="user" action="/registration">
                <br>

                <div class="form-group">
                    <label for="name">First Name</label>
                    <form:input id="name" cssClass="form-control" path="name" type="text"/>
                </div>

                <div class="form-group">
                    <label for="surname">Last Name</label>
                    <form:input id="surname" cssClass="form-control" path="surname" type="text"/>
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <form:input id="email" cssClass="form-control" path="email" type="text"/>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <p class="bg-danger"><form:errors path="password"/></p>
                    <form:input id="password" path="password" cssClass="form-control" type="password"/>
                </div>

                <div class="form-group">
                    <label for="confirmPassword">Confirm password</label>
                    <p class="bg-danger"><form:errors path="password"/></p>
                    <form:input id="confirmPassword" path="confirmPassword" cssClass="form-control" type="password"/>
                </div>

                <%--<div class="form-group">--%>
                    <%--<label for="password">Confirm Password</label>--%>
                    <%--<p class="bg-danger"><form:errors path="password"/></p>--%>
                    <%--<form:input id="password" path="passwordRepeat" cssClass="form-control" type="password"/>--%>
                <%--</div>--%>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Registration</button>
            </form:form>
            <div class="col-sm-4">

            </div>
        </div>
        </div>
    </div>
    <br>


</body>
</html>