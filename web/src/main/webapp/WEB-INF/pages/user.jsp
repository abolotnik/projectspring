<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <title>WaterShop</title>
</head>
<body>
<h1>WaterShop</h1>

<table style="width: 1061px; height: 466px;">
    <tbody>
    <tr>
        <td style="width: 152px;">
        </td>
        <td style="width: 656px;" colspan="1" rowspan="3"
            valign="top">
            <table style="height: 82px; width: 654px;">
                <tr>
                    <th style="height: 60px; width: 60px;">order_id</th>
                    <th style="height: 60px; width: 100px;">Mark</th>
                    <th style="height: 60px; width: 100px;">Type</th>
                    <th style="height: 60px; width: 60px;">Scope</th>
                    <th style="height: 60px; width: 60px;">Count</th>
                    <th style="height: 60px; width: 200px;">Date</th>
                    <th style="height: 60px; width: 150px;">Status</th>


                </tr>

                    <c:set var="lst" value="${list}"/>
                    <c:if test="${not empty lst}">

                    <c:forEach var="lst" items="${list}">
                    <form action="changeorder">
                        <tr>
                            <td style="height: 60px; width: 60px;"><input type="radio" name="id" value="${lst.id}" checked>${lst.id}</td>
                            <td style="height: 60px; width: 100px;"><c:out value="${lst.mark}"/></td>
                            <td style="height: 60px; width: 100px;"><c:out value="${lst.type}"/></td>
                            <td style="height: 60px; width: 60px;"><c:out value="${lst.scope}"/></td>
                            <td style="height: 60px; width: 60px;"><c:out value="${lst.count}"/></td>
                            <td style="height: 60px; width: 200px;"><c:out value="${lst.date}"/></td>
                            <td style="height: 60px; width: 150px;"><c:out value="${lst.status}"/></td>
                            <td><input type="submit" value="change"></td>
                            <td><input type="submit" value="delete" formaction="deleteorder"></td>
                        </tr>
                </form>
                </c:forEach>
                </c:if>
            </table>
        </td>
        <td style="width: 231px;" valign="top">
            <c:if test="${not empty login}">Hi, ${login}
                <form action="logout">
                    <input type="submit" value="Log out">
                </form>
            </c:if>
            <c:if test="${empty login}">Hi, Guest</c:if>


        </td>
    </tr>
    <tr>
        <td style="width: 152px;" valign="top">


            <form action="neworder">
                <input value="New order" type="submit">
            </form>
        </td>
        <td style="width: 231px;">
        </td>
    </tr>
    <tr>
        <td style="width: 152px;"></td>
        <td style="width: 231px;"></td>
    </tr>
    </tbody>
</table>
</body>
</html>
