<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--<%@ page isELIgnored="false" %>--%>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>


<div class="container-fluid">
    <div class="col-sm-12">
        <div class="container-fluid text-center">
            <%--<form:form method="post" modelAttribute="users" action="admin/users">--%>
            <table class="table table-striped">
                <thead>
                <c:forEach var="basket" items="${baskets}">
                <form:form method="post" modelAttribute="basket" action="/user/basket/delete_item/${basket.id}">
                <tr>
                    <td>
                        <form:label path="name">
                            <c:out value="${basket.name}"/>
                        </form:label>
                    </td>
                    <td>
                        <form:label path="photoUrl">
                            <c:out value="${basket.photoUrl}"/>
                        </form:label>
                    </td>
                    <td>
                        <form:label path="description">
                            <c:out value="${basket.description}"/>
                        </form:label>
                    </td>
                    <td>
                        <form:label path="count">
                            <c:out value="${basket.count}"/>
                        </form:label>
                    </td>

                    <td>
                        <form:label path="price">
                            <c:out value="${basket.price}"/>
                        </form:label>
                    </td>
                    <td>
                        <div class="row">
                        <button class="btn btn-lg btn-primary btn-block" formmethod="get" formaction="/user/basket/change_item/${basket.id}" value="Change">
                        <span class="glyphicon glyphicon-wrench"/>Change</button>
                        <button class="btn btn-lg btn-primary btn-block" formaction="/user/basket/delete_item/${basket.id}" value="Delete">
                        <span class="glyphicon glyphicon-remove"/>Delete</button>
                    </div>

                        <%--<a href="/user/basket/change_item/${item.id}"><span class="glyphicon glyphicon-wrench">--%>
                            <%--</span>Change item</a>--%>
                        <%--<a href="/user/basket/delete_item/${item.id}"><span class="glyphicon glyphicon-remove">--%>
                            <%--</span>Delete item</a>--%>
                    </td>
                </tr>
                </form:form>

                    <%--<tr>--%>
                    <%--<td><c:out value="${basket.name}"/></td>--%>
                    <%--<td><c:out value="${basket.photoUrl}"/></td>--%>
                    <%--<td><c:out value="${basket.description}"/></td>--%>
                    <%--<td><c:out value="${basket.count}"/></td>--%>
                    <%--<td><c:out value="${basket.price}"/></td>--%>
                    <%--<td>--%>
                    <%--<a href="/user/basket/change_item/${item.id}"><span class="glyphicon glyphicon-wrench">--%>
                    <%--</span>Change item</a>--%>
                    <%--<a href="/user/basket/delete_item/${item.id}"><span class="glyphicon glyphicon-remove">--%>
                    <%--</span>Delete item</a>--%>
                    <%--</td>--%>
                    <%--</tr>--%>
                </c:forEach>
            </table>
            <div class="container text-right">
                <h2>Total price:</h2>
                <div class="panel panel-default">
                    <div class="panel-body">${totalPrice}<span class="glyphicon glyphicon-usd"/></div>
                    <a href="/user/new_order"><span class="glyphicon glyphicon-piggy-bank"></span>Buy</a>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>


