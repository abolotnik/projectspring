<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--<%@ page isELIgnored="false" %>--%>
<html>
<head>
    <title>Online_Shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Logo</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar1">

            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="/user/catalog">Catalog</a></li>
                <li><a href="/user/news">News</a></li>
                <li><a href="/user/orders">Stores</a></li>
                <li><a href="/user/about_us">Contacts</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/user/profile"><span class="glyphicon glyphicon-user"></span>Profile</a></li>
                <li><a href="/user/basket"><span class="glyphicon glyphicon-shopping-cart"></span> Bascket</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron">
    <div class="container text-center">
        <h1>Online Store</h1>
        <p>Mission, Vission & Values</p>
    </div>
</div>

<div class="container-fluid">
    <div class="col-sm-12">
        <div class="container-fluid text-center">
            <%--<form:form method="post" modelAttribute="users" action="admin/users">--%>
            <%--<form:form  modelAttribute="item" method="post">--%>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Photo</th>
                    <th>Description</th>
                    <th>Count</th>
                    <th colspan="2">Price</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach var="item" items="${catalog}">
                    <form:form method="post" modelAttribute="item" action="/user/catalog/add_item/${item.id}">
                        <tr>
                            <td>
                                <form:label path="id">
                                    <c:out value="${item.id}"/>
                                </form:label>
                            </td>
                            <td>
                                <form:label path="name">
                                    <c:out value="${item.name}"/>
                                </form:label>
                            </td>
                            <td>
                                <form:label path="photoUrl">
                                    <c:out value="${item.photoUrl}"/>
                                </form:label>
                            </td>
                            <td>
                                <form:label path="description">
                                    <c:out value="${item.description}"/>
                                </form:label>
                            </td>
                            <td>
                                <form:label path="count">
                                    <form:input path="count" id="count" placeholder="${item.count}" type="number"/>
                                </form:label>
                            </td>

                            <td>
                                <form:label path="price">
                                    <c:out value="${item.price}"/>
                                </form:label>
                                    <%--<c:out value="${item.price}"/>--%>
                                    <%--<form:input path="count" id="count" type="number" placeholder="${item.count}"/>--%>
                            </td>
                            <td>
                                <button class="btn btn-lg btn-primary btn-block" type=submit">
                                    <span class="glyphicon glyphicon-shopping-cart"></span></button>
                                    <%--<a href="/user/catalog/add_item/${item.id}"><span--%>
                                    <%--class="glyphicon glyphicon-shopping-cart"></span></a>--%>
                            </td>
                        </tr>
                    </form:form>
                </c:forEach>
                <%--</form:form>--%>
                </tbody>
            </table>
            <%--</form:form>--%>
        </div>
    </div>
</div>


<footer class="container-fluid text-center">

    <p>Online Store Copyright</p>
</footer>

</body>
</html>


