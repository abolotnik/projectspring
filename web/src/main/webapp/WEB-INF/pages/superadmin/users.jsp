
<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--<%@ page isELIgnored="false" %>--%>
<html>
<head>
    <title>Online_Shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>


<jsp:include page="adminnavbar.jsp"/>

<jsp:include page="left_menu.jsp"/>

<div class="container-fluid">
    <div class="container-fluid">
        <div class="col-sm-10">
            <div class="container-fluid text-center">
                <%--<form:form method="post" modelAttribute="users" action="admin/users">--%>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="user" items="${users}">
                        <tr>
                            <td><c:out value="${user.id}"/></td>
                            <td><c:out value="${user.name}"/></td>
                            <td><c:out value="${user.email}"/></td>
                            <td><c:out value="${user.role}"/></td>
                            <td >
                                <c:choose>
                                    <c:when test="${user.status}">
                                        <span class="glyphicon glyphicon-ok-sign"/>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="glyphicon glyphicon-remove-sign"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <div class="btn-group-vertical">
                                    <button type="button" class="btn btn-success btn-xs"
                                            href="/superadmin/users/pass/edit/${user.id}">Change password</button>
                                    <button type="button" class="btn btn-success btn-xs"
                                            href="/superadmin/users/role/edit/${user.id}">Change role</button>
                                    <button type="button" class="btn btn-warning btn-xs"
                                            href="/superadmin/users/block/${user.id}">Block</button>
                                    <button type="button" class="btn btn-danger btn-xs"
                                            href="/superadmin/users/delete/${user.id}">Delete</button>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <%--</form:form>--%>
            </div>
        </div>
    </div>


<footer class="container-fluid text-center">

    <p>Online Store Copyright</p>
</footer>

</body>
</html>


