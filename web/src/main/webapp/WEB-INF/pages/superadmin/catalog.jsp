<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--<%@ page isELIgnored="false" %>--%>
<html>
<head>
    <title>Online_Shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>


<jsp:include page="adminnavbar.jsp"/>

<jsp:include page="left_menu.jsp"/>
    <div class="col-sm-10">
        <div class="container-fluid text-center">
            <%--<form:form method="post" modelAttribute="users" action="admin/users">--%>
            <%--<form:form  modelAttribute="item" method="post">--%>
                <p class="bg-danger"><c:out value="${message}"/> </p>
                <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Photo</th>
                    <th>Description</th>
                    <th>Count</th>
                    <th colspan="2">Price</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach var="items" items="${items}">
                    <form:form method="post" modelAttribute="item" action="/admin/catalog/copy/${items.id}">
                        <tr>
                            <td>
                                <%--<label for="id">Id</label>--%>
                                <c:out value="${items.id}"/>
                                <form:input id="id" cssClass="form-control" path="id" type="hidden"/>
                            </td>
                            <td>

                                <%--<label for="name">Name</label>--%>
                                <c:out value="${items.name}"/>

                                <form:input id="name" cssClass="form-control" path="name" type="hidden"/>

                                <%--<form:label path="name">--%>
                                    <%--<c:out value="${item.name}"/>--%>
                                <%--</form:label>--%>
                            </td>
                            <td>

                                <%--<label for="photoUrl">Photo</label>--%>
                                <c:out value="${items.photoUrl}"/>
                                <form:input id="name" cssClass="form-control" path="photoUrl" type="hidden"/>

                                <%--<form:label path="photoUrl">--%>
                                    <%--<c:out value="${item.photoUrl}"/>--%>
                                <%--</form:label>--%>
                            </td>
                            <td>

                                <%--<label for="description">Description</label>--%>
                                <c:out value="${items.description}"/>
                                <form:input id="description" cssClass="form-control" path="description"
                                            placeholder="${items.description}" type="hidden"/>

                                <%--<form:label path="description">--%>
                                    <%--<c:out value="${item.description}"/>--%>
                                <%--</form:label>--%>
                            </td>
                            <td>
                                <label for="count"></label>
                                <%--<label for="count">Count</label>--%>
                                <c:out value="${items.count}"/>
                                <form:input id="count" cssClass="form-control" path="count" type="hidden"/>

                                <%--<form:label path="count">--%>
                                    <%--<form:input path="count" id="count" placeholder="${item.count}" type="number"/>--%>
                                <%--</form:label>--%>
                            </td>

                            <td>
                                <label for="price"></label>
                                <c:out value="${items.price}"/>
                                <form:input id="price" cssClass="form-control" path="price" type="hidden"/>
                                <%--<label for="price">Price</label>--%>
                                <%--<c:out value="${items.price}"/>--%>
                                <%--<form:input id="price" cssClass="form-control" path="price" type="text"/>--%>

                                <%--<form:label path="price">--%>
                                    <%--<c:out value="${item.price}"/>--%>
                                <%--</form:label>--%>
                                    <%--<c:out value="${item.price}"/>--%>
                                    <%--<form:input path="count" id="count" type="number" placeholder="${item.count}"/>--%>
                            </td>
                            <td>
                                <a href="/admin/catalog/edit/${items.id}"><span
                                        class="glyphicon glyphicon-edit"></span>Edit</a>
                                <a href="/admin/catalog/delete/${items.id}"><span
                                        class="glyphicon glyphicon-trash"></span>Delete</a>
                            </td>
                        </tr>
                    </form:form>
                </c:forEach>
                <%--</form:form>--%>
                </tbody>
            </table>
            <%--</form:form>--%>
        </div>
    </div>
</div>


<footer class="container-fluid text-center">

    <p>Online Store Copyright</p>
</footer>

</body>
</html>


