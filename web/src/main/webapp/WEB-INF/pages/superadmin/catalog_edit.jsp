<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--<%@ page isELIgnored="false" %>--%>
<html>
<head>
    <title>Online_Shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>


<jsp:include page="adminnavbar.jsp"/>

<jsp:include page="left_menu.jsp"/>
    <div class="col-sm-10">
        <div class="container-fluid text-center">
            <%--<form:form method="post" modelAttribute="users" action="admin/users">--%>
            <%--<form:form  modelAttribute="item" method="post">--%>
            <p class="bg-danger"><c:out value="${message}"/> </p>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Photo</th>
                    <th>Description</th>
                    <th>Count</th>
                    <th colspan="2">Price</th>
                </tr>
                </thead>
                <tbody>

                    <form:form method="post" modelAttribute="item" action="/admin/catalog/edit/${item.id}">
                        <tr>
                            <td>
                                <c:out value="${item.id}"/>
                                <form:input id="id" cssClass="form-control" path="id" type="hidden"/>
                            </td>
                            <td>
                                <c:out value="${item.name}"/>
                                <form:input id="name" cssClass="form-control" path="name" type="hidden"/>
                            </td>
                            <td>
                                <c:out value="${item.photoUrl}"/>
                                <form:input id="name" cssClass="form-control" path="photoUrl" type="hidden"/>
                            </td>
                            <td>
                                <c:out value="${item.description}"/>
                                <form:input id="description" cssClass="form-control" path="description"
                                            placeholder="${item.description}" type="hidden"/>
                            </td>
                            <td>
                                <label for="count"></label>
                                <c:out value="${item.count}"/>
                                <form:input id="count" cssClass="form-control" path="count" type="hidden"/>
                            </td>

                            <td>
                                <label for="price">Price</label>
                                <c:out value="${item.price}"/>
                                <form:input id="price" cssClass="form-control" path="price" type="text"/>
                            </td>
                            <td>
                                <button class="btn btn-default">Submit</button>
                            </td>
                        </tr>
                    </form:form>
                </tbody>
            </table>
        </div>
    </div>
</div>


<footer class="container-fluid text-center">

    <p>Online Store Copyright</p>
</footer>

</body>
</html>


