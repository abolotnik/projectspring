<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Online_Shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>


<jsp:include page="adminnavbar.jsp"/>

<jsp:include page="left_menu.jsp"/>
<div class="col-md-10">
<div class="row">

<%--enctype="multipart/form-data"--%>
<div class="container-fluid">
<form:form method="post" modelAttribute="news" action="/admin/news/add">
    <div class="form-group">
        <label for="title">Title</label>
        <p class="bg-danger"><form:errors path="title"/></p>
        <form:input id="title" cssClass="form-control" path="title" type="text"/>
    </div>
    <div class="form-group">
        <label for="content">News content</label>
        <p class="bg-danger"><form:errors path="content"/></p>
        <form:input id="content" cssClass="form-control" path="content" type="text"/>
    </div>
    <div class="form-group">
        <label for="photoUrl"><span class="glyphicon glyphicon-floppy-save">Chose images...</span></label>
        <p class="bg-danger"><form:errors path="photoUrl"/></p>

        <form:input id="photoUrl" path="photoUrl" cssClass="form-control" type="file"/>
    </div>
    <button class="btn btn-default">Submit</button>
</form:form>
    </div>
    </div>
    <footer class="container-fluid text-center">

    <p>Online Store Copyright</p>
    </footer>

    </body>
    </html>


