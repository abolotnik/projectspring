<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--<%@ page isELIgnored="false" %>--%>
<html>
<head>
    <title>Online_Shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>


<jsp:include page="adminnavbar.jsp"/>

<jsp:include page="left_menu.jsp"/>
        <div class="col-sm-10">
            <div class="row">
                <%--Generated tiles of news--%>
                <div class="container-fluid">
                    <c:forEach var="news" items="${news}">
                        <div class="col-sm-3">
                            <div class="panel panel-primary">
                                    <%--<input type="hidden" name="id" value="${news.id}"/>--%>
                                <div class="panel-heading">${news.title}</div>
                                        <div class="panel-body">${news.content}</div>
                                <div class="panel-footer">
                                        <%--<form:button modelAttribute="news" id="${news.id}" cssClass="btn btn-primary"/>--%>
                                    <%--<button type="button" class="btn btn-primary"--%>
                                            <%--formaction="/admin/news/change/${news.id}">--%>
                                        <%--<span class="glyphicon glyphicon-edit">Edit</span>--%>
                                    <%--</button>--%>
                                    <a href="/admin/news/edit/${news.id}"><span
                                            class="glyphicon glyphicon-edit"></span>Edit</a>
                                            <a href="/admin/news/delete/${news.id}"><span
                                                    class="glyphicon glyphicon-remove"></span>Delete</a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="container-fluid text-center">

    <p>Online Store Copyright</p>
</footer>

</body>
</html>
