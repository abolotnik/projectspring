<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Online_Shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>


<jsp:include page="adminnavbar.jsp"/>

<jsp:include page="left_menu.jsp"/>

<div class="col-md-10">
    <div class="row">

        <%--enctype="multipart/form-data"--%>
        <div class="container-fluid">
            <form:form method="post" modelAttribute="user" action="/superadmin/user/role/edit/${user.id}">
                <div class="form-group">
                    <label for="name">Name</label>
                    <p class="bg-danger"><form:errors path="name"/></p>
                    <c:out value="${user.name}"/>
                    <form:input id="title" cssClass="form-control" path="name" type="hidden"/>
                    <form:input id="id" cssClass="form-control" path="id" type="hidden"/>
                </div>
                <div class="form-group">
                    <label for="surname">Surname</label>
                    <p class="bg-danger"><form:errors path="surname"/></p>
                    <form:input id="content" cssClass="form-control" path="surname" type="text"/>
                </div>
                <div class="form-group">
                    <label for="surname">Email</label>
                    <p class="bg-danger"><form:errors path="email"/></p>
                    <form:input id="email" cssClass="form-control" path="email" type="hidden"/>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <p class="bg-danger"><form:errors path="password"/></p>
                    <form:input id="password" cssClass="form-control" path="password" type="hidden"/>
                </div>
                <div class="form-group">
                    <label for="role">Role</label>
                    <p class="bg-danger"><form:errors path="password"/></p>
                    <form:input id="role" cssClass="form-control" path="role" type="role">
                                    <select name="role">
                                        <c:forEach var="role" items="${roles}">
                                        <option value="${role}">${role}</option>
                                        </c:forEach>
                                    </select>

                    </form:input>
                </div>
                <button class="btn btn-default">Submit</button>
            </form:form>

        </div>
    </div>
    <footer class="container-fluid text-center">

        <p>Online Store Copyright</p>
    </footer>

</body>
</html>


