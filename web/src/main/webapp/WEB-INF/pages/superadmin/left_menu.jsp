<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page isELIgnored="false" %>
<html>
<head>

<div class="col-md-2">
    <nav class="navbar">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="myNavbar2">
                <ul class="nav navbar-nav">
                    <li><a href="/admin/news/add">Add news</a></li>
                    <li><a href="/superadmin/user/add">Add user</a></li>
                    <li><a href="/admin/catalog/add">Add item</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
</head>



</body>
</html>


