<%@ page contentType="text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--<%@ page isELIgnored="false" %>--%>
<html>
<head>
    <title>Online_Shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Logo</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar1">

            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="/user/catalog">Catalog</a></li>
                <li><a href="/user/news">News</a></li>
                <li><a href="/user/orders">Stores</a></li>
                <li><a href="/user/about_us">Contacts</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/user/profile"><span class="glyphicon glyphicon-user"></span>Profile</a></li>
                <li><a href="/user/basket"><span class="glyphicon glyphicon-shopping-cart"></span> Bascket</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron">
    <div class="container text-center">
        <h1>Online Store</h1>
        <p>Mission, Vission & Values</p>
    </div>
</div>

<div class="container-fluid">
    <div class="col-sm-12">
        <div class="row">
            <%--Generated tiles of news--%>
            <div class="container-fluid">
                <c:forEach var="news" items="${news}">
                    <div class="col-sm-3">
                        <div class="panel panel-primary">
                                <%--<input type="hidden" name="id" value="${news.id}"/>--%>
                            <div class="panel-heading">${news.title}</div>
                            <div class="panel-footer">
                                    <%--<img src="${pageContext.request.contextPath}${news.photoUrl}"--%>
                                    <%--alt="Smiley face"--%>
                                    <%--height="80" width="60"></div>--%>
                                    <%--<form:button modelAttribute="news" id="${news.id}" cssClass="btn btn-primary"/>--%>
                                <button type="button" class="btn btn-primary"><span
                                        class="glyphicon glyphicon-sunglasses"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>

<footer class="container-fluid text-center">

    <p>Online Store Copyright</p>
</footer>

</body>
</html>
