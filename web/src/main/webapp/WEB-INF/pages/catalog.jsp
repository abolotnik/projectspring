<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Catalog</title>
</head>
<body>
        <form action="neworder" method="post">
            <fieldset>
                <legend>Please, make new order</legend>
            <select name="mark">
                <option value="202">202</option>
                <option value="Borovaja">Borovaja</option>
                <option value="Minskaja">Minskaja</option>
            </select>

            <select name="type">
                <option value="still">still</option>
                <option value="mineralize">mineralize</option>
                <option value="carbonated">carbonated</option>
            </select>

            <select name="scope">
                <option value="1">1</option>
                <option value="3">3</option>
                <option value="5">5</option>
                <option value="9">9</option>
                <option value="19">19</option>
            </select>

            <input name="count" value="1" min="1" max="20"
                   step="1" type="number">
            <input value="Add" type="submit">
            </fieldset>
        </form>
        <form action="showorders" method="post">
            <input type="submit" name="back" value="Back">
        </form>



</body>
</html>

