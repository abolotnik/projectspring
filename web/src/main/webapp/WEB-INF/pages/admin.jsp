<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>WaterShop</title>
</head>
<body>
<h1>WaterShop</h1>

<table style="width: 1061px; height: 466px;">
    <tbody>
    <tr style=" width: 25%">
        <td style="width: 152px;">
        </td>
        <td style="width: 656px;" colspan="1" rowspan="3"
            valign="top">
            <table style="height: 82px; width: 654px;">
                <tr>
                    <th >order_id</th>
                    <th >Login</th>
                    <th>Mark</th>
                    <th >Type</th>
                    <th>Scope</th>
                    <th>Count</th>
                    <th>Date</th>
                    <th>Status</th>
                </tr>
                <c:if test="${not empty list}">

                    <c:forEach var="lst" items="${list}">
                        <form action="changeorder">
                            <tr style="height: 60px; width: 60%">
                                <td style="width: auto"><input type="radio" name="id" value="${lst.id}" checked>${lst.id}</td>
                                <td style="width: auto"><c:out value="${lst.login}"/></td>
                                <td style="width: auto"><c:out value="${lst.mark}"/></td>
                                <td style="width: auto"><c:out value="${lst.type}"/></td>
                                <td style="width: auto"><c:out value="${lst.scope}"/></td>
                                <td style="width: auto"><c:out value="${lst.count}"/></td>
                                <td style="width: auto"><c:out value="${lst.date}"/></td>
                                <td style="width: auto"><c:out value="${lst.status}"/></td>
                                <td><input type="submit" value="change"></td>
                                <td><input type="submit" value="delete" formaction="deleteorder"></td>
                            </tr>
                        </form>
                    </c:forEach>
                </c:if>
            </table>
        </td>
        <td style="width: 231px;" valign="top">
            <c:if test="${not empty login}">Hi, ${login}
                <form action="logout">
                    <input type="submit" value="Log out">
                </form>
            </c:if>
            <c:if test="${empty login}">Hi, Guest</c:if>


        </td>
    </tr>
    <tr>
        <td style="width: 152px;">
        </td>
        <td style="width: 231px;">
        </td>
    </tr>
    <tr>
        <td style="width: 152px;"></td>
        <td style="width: 231px;"></td>
    </tr>
    </tbody>
</table>
</body>
</html>
