<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Catalog</title>
</head>
<body>
<form action="changeorder" method="post">
    <fieldset>
        <legend>Please, make new order</legend>
        <select>
            <c:set var="order" scope="request" value="${order}"/>
            <option value="${order.id}"><c:out value="${order.id}"/></option>
        </select>
        <select name="status">
            <option value="in processing">in processing</option>
            <option value="get to driver">get to driver</option>
            <option value="wait delivery">wait delivery</option>
            <option value="arrived">arrived</option>
            <option value="delivered">delivered</option>
        </select>
        <input value="Change" type="submit">
    </fieldset>
</form>


</body>
</html>


