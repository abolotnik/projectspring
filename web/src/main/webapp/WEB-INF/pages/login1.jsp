<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Login</title>
</head>
<body>

<form action="login" method="post">
<fieldset>
<legend>Please, enter your email and password below:</legend>
<pre>Email: </pre>
<c:if test="not empty ${loginmessage}">
<c:out value="${loginmessage}"/>
</c:if>
    <br>
    <input type="text" name="email" placeholder="example@email.com">
    <pre>Password: </pre> <c:out value="${loginmessage}"/>
    <br>
    <input type="text" name="password" placeholder="qwerty">
    <br><br>
    <input type="submit" value="Login">
    </fieldset>
    </form>
<form action="registration" method="get">
    <input type="submit" value="Registration" >
</form>
    </body>
    </html>
