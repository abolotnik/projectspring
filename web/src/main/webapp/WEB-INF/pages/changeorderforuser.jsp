<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Catalog</title>
</head>
<body>
<form action="changeorder" method="post">
    <fieldset>
        <legend>Please, make new order</legend>
        <select>
            <c:set var="order" scope="request" value="${order}"/>
            <option value="${order.id}"><c:out value="${order.id}"/></option>
        </select>


        <select name="scope">
            <option value="1">1</option>
            <option value="3">3</option>
            <option value="5">5</option>
            <option value="9">9</option>
            <option value="19">19</option>
        </select>

        <input name="count" min="1" max="20"
               step="1" type="number" value="1">
        <input value="Change" type="submit">
    </fieldset>
</form>


</body>
</html>


