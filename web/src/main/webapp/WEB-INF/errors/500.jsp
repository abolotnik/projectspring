<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Error 500</title>
</head>
<body>
<h1>Error 500</h1>
<p>Где-то что-то потерялось. Мы уже ищем...</p>
Request from ${pageContext.errorData.requestURI} is failed<br>
Servlet name: ${pageContext.errorData.servletName}<br>
Status code: ${pageContext.errorData.statusCode}<br>
Exception: ${pageContext.exception}<br>
Message from exception: ${pageContext.exception.message}
</body>
</html>
